from dataclasses import dataclass
from typing import Dict

from .base import BaseImportEntity
from ..models import (
    PayerType,
    CargoType
)

__all__ = (
    'Document',
    'DocumentStatus',
    'DocumentPrice',
    'DocumentDeliveryDate',
    'BackwardDeliveryData',
)


@dataclass
class Document(BaseImportEntity):
    ref: str
    cost_on_site: str
    estimated_delivery_date: str
    int_doc_number: str
    type_document: str
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


@dataclass
class DocumentStatus(BaseImportEntity):
    number: str
    status: str
    status_code: str
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


@dataclass
class DocumentPrice(BaseImportEntity):
    assessed_cost: int
    cost: int
    cost_redelivery: int
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


@dataclass
class DocumentDeliveryDate(BaseImportEntity):
    date: str
    timezone_type: int
    timezone: str
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


@dataclass
class BackwardDeliveryData:
    payer_type: PayerType
    cargo_type: 'str'
    redelivery_string: str = ''

    @property
    def as_data(self):
        return {
            "PayerType": self.payer_type.ref,
            "CargoType": self.cargo_type,
            "RedeliveryString": self.redelivery_string
        }
