from dataclasses import dataclass
from typing import Dict

from .base import BaseImportEntity

__all__ = (
    'ContactPerson',
)


@dataclass
class ContactPerson(BaseImportEntity):
    ref: str
    description: str
    first_name: str
    middle_name: str
    last_name: str
    phones: str = None
    email: str = None
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
