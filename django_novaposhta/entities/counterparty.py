from dataclasses import dataclass
from typing import Dict

from .base import BaseImportEntity
from .contact_person import ContactPerson
from ..models import (
    CounterpartyType as CounterpartyTypeModel,
    City as CityModel
)

__all__ = (
    'Counterparty',
)


@dataclass
class Counterparty(BaseImportEntity):
    ref: str
    description: str
    first_name: str
    middle_name: str
    last_name: str
    city: str = None
    counterparty: str = None
    counterparty_type: str = None
    contact_person: Dict = None
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.contact_person:
            self.contact_person = (
                ContactPerson(
                    **self.contact_person['data'][0]
                )
                .as_dict()
            )

        if self.counterparty_type:
            self.counterparty_type = (
                CounterpartyTypeModel.objects.get(ref=self.counterparty_type)
            )

        if self.city:
            try:
                self.city = (
                    CityModel.objects.get(ref=self.city)
                )
            except CityModel.DoesNotExist:
                self.city = None
