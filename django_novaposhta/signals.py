from django.dispatch import Signal

__all__ = (
    'api_request_signal',
)


api_request_signal = Signal()
