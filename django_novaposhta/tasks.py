from celery import shared_task

from .services import (
    populate_warehouses_with_settlement_ref,
    populate_warehouses_with_settlement_ref2,
    track_actual_documents,
)
from .shortcuts import update_novaposhta

__all__ = (
    'update_novaposhta_task',
    'populate_warehouses_with_settlement_ref_task',
    'populate_warehouses_with_settlement_ref2_task',
    'update_warehouses_settlement_refs'
)


@shared_task(bind=True,)
def update_novaposhta_task(self):
    update_novaposhta()


@shared_task(
    bind=True,
    # default_retry_delay=1 * 60,
    autoretry_for=(Exception,),
    retry_kwargs={'max_retries': 5},
    retry_backoff=True
)
def populate_warehouses_with_settlement_ref_task(self):
    populate_warehouses_with_settlement_ref()


@shared_task(
    bind=True,
    # default_retry_delay=1 * 60,
    autoretry_for=(Exception,),
    retry_kwargs={'max_retries': 5},
    retry_backoff=True
)
def populate_warehouses_with_settlement_ref2_task(self, settlement_ref: str):
    populate_warehouses_with_settlement_ref2(settlement_ref=settlement_ref)


@shared_task(bind=True)
def update_warehouses_settlement_refs(self):
    from .models import Settlement

    for settlement in Settlement.objects.all():
        populate_warehouses_with_settlement_ref2_task.delay(settlement.ref)


@shared_task(bind=True)
def track_actual_documents_task():
    track_actual_documents()
