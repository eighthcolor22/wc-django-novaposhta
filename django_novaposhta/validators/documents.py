from decimal import Decimal
from typing import TYPE_CHECKING, List

from django.utils.translation import pgettext_lazy

from ..consts import (
    WAREHOUSE_PACKSTATION_TYPE_REF,
    DOCUMENT_TRACKING_DEFAULT_STATUS,
)
from ..exceptions import NovaposhtaValidationError
from ..entities.address import WarehouseLimitations

if TYPE_CHECKING:
    from ..models import Warehouse

__all__ = (
    'validate_option_seat',
    'validate_documents_scan_sheet_allowed',
)


def _get_warehouse_limitations(warehouse: 'Warehouse'):
    DEFAULT_DIMENSIONS_VALUE = 1000  # To avoid exception in comparing when warehouse row_data does
    # not include some parameter. It may be changed.

    dimensions = warehouse.raw_data.get('ReceivingLimitationsOnDimensions')
    weight_allowed = warehouse.raw_data.get('PlaceMaxWeightAllowed')
    if not weight_allowed or weight_allowed == '0':
        weight_allowed = warehouse.raw_data.get('TotalMaxWeightAllowed')
    if not weight_allowed or weight_allowed == '0':
        weight_allowed = DEFAULT_DIMENSIONS_VALUE
    return WarehouseLimitations(
        total_weight=Decimal(weight_allowed),
        length=Decimal(dimensions.get('Length', DEFAULT_DIMENSIONS_VALUE)),
        width=Decimal(dimensions.get('Width', DEFAULT_DIMENSIONS_VALUE)),
        height=Decimal(dimensions.get('Height', DEFAULT_DIMENSIONS_VALUE)),
    )


def validate_option_seat(
    option_seat: List,
    warehouse: 'Warehouse'
):
    option_seat_count = len(option_seat)
    if (
        option_seat_count > 1 and
        warehouse.type_of_warehouse.ref == WAREHOUSE_PACKSTATION_TYPE_REF
    ):
        raise NovaposhtaValidationError(
            pgettext_lazy(
                'django_novaposhta',
                'It is allowed only one place to packstation warehouse type'
            )
        )

    warehouse_limitations = _get_warehouse_limitations(warehouse)
    for place in option_seat:
        is_valid = warehouse_limitations.validate_dimensions(place)

        if not is_valid:
            raise NovaposhtaValidationError(
                pgettext_lazy('django_novaposhta', 'Warehouse limitations error')
            )


def validate_documents_scan_sheet_allowed(documents: List['Document']):
    for document in documents:
        if document.status != DOCUMENT_TRACKING_DEFAULT_STATUS:
            raise NovaposhtaValidationError(
                pgettext_lazy('django_novaposhta', 'Only pending status allowed')
            )

        if document.pdf or document.marking_pdf:
            raise NovaposhtaValidationError(
                pgettext_lazy('django_novaposhta', 'Printed documents are not allowed')
            )
