from typing import Optional, Union

from django.contrib.auth import get_user_model
from django.utils.translation import pgettext_lazy

from ..consts import (
    WAREHOUSE_DEPARTURE_SERVICE_TYPES_MAP,
    DOORS_DEPARTURE_SERVICE_TYPES_MAP,
    DEFAULT_SERVICE_TYPE,
)
from ..exceptions import (
    NovaposhtaException,
    NovaposhtaValidationError,
)
from ..models import UserPreferences, Preferences, ServiceType
from ..services.sender import populate_sender_preferences

__all__ = (
    'validate_user_preferences_exists',
    'validate_preferences_service_type',
    'populate_user_preferences',
)

UserModel = get_user_model()


def validate_user_preferences_exists(user: 'UserModel'):
    user_preferences_exists = (
        UserPreferences.objects.filter(user=user).exists()
    )
    if user_preferences_exists:
        raise NovaposhtaValidationError(
            pgettext_lazy('django_novaposhta', 'User Preferences already exists')
        )


def validate_preferences_service_type(
        cleaned_data: dict,
        instance: Union['UserPreferences', 'Preferences', None] = None
):
    service_type = (
            cleaned_data.get('service_type') or
            instance and instance.service_type
    )

    if service_type and service_type.ref in WAREHOUSE_DEPARTURE_SERVICE_TYPES_MAP.values():
        city = cleaned_data.get('city') or instance.city
        warehouse = cleaned_data.get('warehouse')
        if not city or not warehouse:
            raise NovaposhtaValidationError(
                pgettext_lazy(
                    'django_novaposhta', 'Warehouse service type requires city and warehouse'
                )
            )
        if warehouse.city_id != city.id:
            raise NovaposhtaValidationError(
                pgettext_lazy(
                    'django_novaposhta', 'Chosen warehouse does not exist in chosen city'
                )
            )

    elif service_type and service_type.ref in DOORS_DEPARTURE_SERVICE_TYPES_MAP.values():
        city = cleaned_data.get('city') or instance and instance.city
        street = cleaned_data.get('street') or instance and instance.street
        building = cleaned_data.get('building') or instance and instance.building

        required_values_filled = city and street and building
        if not required_values_filled:
            raise NovaposhtaValidationError(
                pgettext_lazy(
                    'django_novaposhta', 'Door service type requires city street and building'
                )
            )
        if street.city_id != city.id:
            raise NovaposhtaValidationError(
                pgettext_lazy(
                    'django_novaposhta', 'Chosen street does not exist in chosen city'
                )
            )


def populate_user_preferences(
        attrs: dict,
        instance: Optional[UserPreferences]
):
    """
    provides validation api_key while receiving and population user preferences object with
    novaposhta sender instances: counterparty, contact person, sender address
    Returns:
        error msg or UserPreferences instance
    """
    if not instance:
        instance = UserPreferences(**attrs)
    else:
        for key, val in attrs.items():
            setattr(instance, key, val)
    try:
        populate_sender_preferences(instance, save=True)
        return instance
    except NovaposhtaException:
        raise NovaposhtaValidationError(
            pgettext_lazy(
                'django_novaposhta', 'Something went wrong. '
                                     'Please, check entered data or try a bit later.'
            )
        )

