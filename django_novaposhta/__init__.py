__version__ = '0.1.1'

VERSION = tuple(__version__.split('.'))

default_app_config = 'django_novaposhta.apps.DjangoNovaposhtaConfig'
