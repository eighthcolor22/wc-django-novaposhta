from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel, AutocompleteSearchFieldMixin

__all__ = (
    "City",
)


class City(
    AutocompleteSearchFieldMixin,
    BaseNovaposhtaModel
):
    description_ru = models.CharField(
        pgettext_lazy('novaposhta', "Description in russian"),
        max_length=50,
        db_index=True
    )
    area = models.ForeignKey(
        'django_novaposhta.Area',
        null=True,
        on_delete=models.CASCADE,
        related_name='cities',
        verbose_name=pgettext_lazy('novaposhta', "Area"),
    )
    settlement_type = models.ForeignKey(
        'django_novaposhta.SettlementType',
        null=True,
        on_delete=models.SET_NULL,
        related_name='cities',
        verbose_name=pgettext_lazy('novaposhta', "Settlement Type"),
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "City")
        verbose_name_plural = pgettext_lazy('novaposhta', "Cities")

    @staticmethod
    def autocomplete_search_fields():
        return 'description',