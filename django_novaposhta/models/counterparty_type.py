from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel

__all__ = (
    "CounterpartyType",
)


class CounterpartyType(BaseNovaposhtaModel):
    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Counterparty type")
        verbose_name_plural = pgettext_lazy('novaposhta', "Counterparty types")
