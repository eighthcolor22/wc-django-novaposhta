from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel, AutocompleteSearchFieldMixin

__all__ = (
    "Area",
)


class Area(
    AutocompleteSearchFieldMixin,
    BaseNovaposhtaModel
):
    description_ru = models.CharField(
        pgettext_lazy('novaposhta', "Description in russian"),
        blank=True,
        max_length=150,
    )
    areas_center = models.OneToOneField(
        'django_novaposhta.City',
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name=pgettext_lazy('novaposhta', "Area center")
    )
    is_active = models.BooleanField(
        pgettext_lazy('novaposhta', "Is active"),
        default=True
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Area")
        verbose_name_plural = pgettext_lazy('novaposhta', "Areas")
