from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import pgettext_lazy

from solo import settings as solo_settings
from solo.models import SingletonModel

from ..consts import (
    API_V2_URL,
    DEFAULT_DESCRIPTION,
    NOVAPOSHTA_MINIMAL_ESTIMATED_COST,
)
from .abstracts import BasePerson

__all__ = (
    'Preferences',
    'UserPreferences',
)


class AbstractPreferences(models.Model):

    api_key = models.CharField(
        pgettext_lazy('novaposhta', "API key"),
        max_length=255,
    )
    sender = models.CharField(
        pgettext_lazy('novaposhta', "Sender"),
        max_length=36,
        blank=True
    )
    sender_city = models.CharField(
        pgettext_lazy('novaposhta', "Sender city"),
        max_length=36,
        blank=True
    )
    sender_address = models.CharField(
        pgettext_lazy('novaposhta', "Sender address"),
        max_length=36,
        blank=True
    )
    sender_contact = models.CharField(
        pgettext_lazy('novaposhta', "Sender contact"),
        max_length=36,
        blank=True
    )
    sender_phone = models.CharField(
        pgettext_lazy('novaposhta', "Sender phone"),
        max_length=36,
        blank=True
    )
    payment_form = models.ForeignKey(
        'django_novaposhta.PaymentForm',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=pgettext_lazy('novaposhta', "Default payment form"),
    )
    service_type = models.ForeignKey(
        'django_novaposhta.ServiceType',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=pgettext_lazy('novaposhta', "Default service type"),
    )
    payer_type = models.ForeignKey(
        'django_novaposhta.PayerType',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=pgettext_lazy('novaposhta', "Default payer type"),
    )
    cargo_type = models.ForeignKey(
        'django_novaposhta.CargoType',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=pgettext_lazy('novaposhta', "Default cargo type"),
    )

    phone = models.CharField(
        verbose_name=pgettext_lazy('novaposhta:preferences', "Phone"),
        help_text=pgettext_lazy('novaposhta:preferences', "Enter phone in format: +38**********."),
        max_length=13, null=True, blank=True
    )
    city = models.ForeignKey(
        'django_novaposhta.City',
        on_delete=models.SET_NULL, null=True, blank=True,
        verbose_name=pgettext_lazy('novaposhta:preferences', "City"),
        help_text=pgettext_lazy('novaposhta:preferences', "Lookup by name and name_ru"),
    )
    street = models.ForeignKey(
        'django_novaposhta.Street',
        on_delete=models.SET_NULL, null=True, blank=True,
        verbose_name=pgettext_lazy('novaposhta:preferences', "Street"),
        help_text=pgettext_lazy('novaposhta:preferences', "Lookup by name, city name and city name_ru"),
    )
    building = models.CharField(
        verbose_name=pgettext_lazy('novaposhta:preferences', "Building"),
        max_length=10, null=True, blank=True,
    )
    flat = models.CharField(
        verbose_name=pgettext_lazy('novaposhta:preferences', "Flat"),
        max_length=10, null=True, blank=True,
    )
    warehouse = models.ForeignKey(
        'django_novaposhta.Warehouse',
        on_delete=models.SET_NULL, null=True, blank=True,
        verbose_name=pgettext_lazy('novaposhta:preferences', "Warehouse"),
        help_text=pgettext_lazy('novaposhta:preferences', "Lookup by number, city name and city name_ru"),
    )

    class Meta:
        abstract = True


class Preferences(
    BasePerson,
    AbstractPreferences,
    SingletonModel
):
    api_url = models.CharField(
        pgettext_lazy('novaposhta', "API url"),
        blank=True,
        default=API_V2_URL,
        max_length=255,
    )

    weight = models.FloatField(
        pgettext_lazy('novaposhta', "Weight"),
        default=0.1,
    )
    seats_amount = models.PositiveIntegerField(
        verbose_name=pgettext_lazy('novaposhta', "Seats amount"),
        default=1
    )
    description = models.TextField(
        verbose_name=pgettext_lazy('novaposhta', "Description"),
        default=DEFAULT_DESCRIPTION
    )
    cost = models.PositiveIntegerField(
        verbose_name=pgettext_lazy('novaposhta', "Cost"),
        validators=(MinValueValidator(300), ),
        default=NOVAPOSHTA_MINIMAL_ESTIMATED_COST
    )

    class Meta:
        verbose_name = pgettext_lazy('novaposhta', "Preferences")

    def __str__(self):
        return str(self._meta.verbose_name)

    @classmethod
    def get_cache_key(cls):
        prefix = getattr(settings, "SOLO_CACHE_PREFIX", solo_settings.SOLO_CACHE_PREFIX)
        return f"{prefix}:django-novaposhta:{cls.__name__.lower()}"


class UserPreferences(
    BasePerson,
    AbstractPreferences
):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='novaposhta_preferences',
        verbose_name=pgettext_lazy('novaposhta:preferences', 'User'),
    )
    is_active = models.BooleanField(
        pgettext_lazy('novaposhta:preferences', 'Is active'),
        default=False
    )

    created_at = models.DateTimeField(
        pgettext_lazy('novaposhta:preferences', 'Created at'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        pgettext_lazy('novaposhta:preferences', 'Updated at'),
        auto_now=True
    )

    class Meta:
        verbose_name = pgettext_lazy('novaposhta:preferences', 'User preferences')
        verbose_name_plural = pgettext_lazy('novaposhta:preferences', 'User preferences')

    def __str__(self):
        return str(self.user)
