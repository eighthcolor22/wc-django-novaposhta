from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel

__all__ = (
    "CargoType",
)


class CargoType(BaseNovaposhtaModel):
    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Cargo type")
        verbose_name_plural = pgettext_lazy('novaposhta', "Cargo types")
