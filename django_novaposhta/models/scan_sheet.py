from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel, AutocompleteSearchFieldMixin
from ..file_storage import novaposhta_upload_storage
from ..utils import file_upload_to

__all__ = (
    "ScanSheet",
)


class ScanSheet(
    AutocompleteSearchFieldMixin,
    BaseNovaposhtaModel
):
    description = None
    number = models.CharField(
        pgettext_lazy('novaposhta', "Number"),
        blank=True,
        max_length=36
    )
    date = models.DateTimeField(
        pgettext_lazy('novaposhta', 'Date'),
        blank=True,
        null=True
    )
    printed = models.IntegerField(
        pgettext_lazy('novaposhta', 'Is scan sheet printed'),
        blank=True,
        null=True
    )
    count = models.IntegerField(
        pgettext_lazy('novaposhta', 'Count of documents'),
        blank=True,
        null=True
    )
    city_sender = models.ForeignKey(
        'django_novaposhta.City',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='sender_scan_sheets',
        verbose_name=pgettext_lazy('novaposhta', "City sender"),
    )
    sender_address = models.CharField(
        pgettext_lazy('novaposhta', "Sender address"),
        blank=True,
        max_length=512
    )
    sender = models.CharField(
        pgettext_lazy('novaposhta', "Sender"),
        blank=True,
        max_length=255
    )
    pdf = models.FileField(
        pgettext_lazy('novaposhta', "PDF"),
        null=True,
        blank=True,
        upload_to=file_upload_to,
        storage=novaposhta_upload_storage,
    )
    marking_pdf = models.FileField(
        pgettext_lazy('novaposhta', "Marking PDF"),
        null=True,
        blank=True,
        upload_to=file_upload_to,
        storage=novaposhta_upload_storage,
    )
    is_dispatched = models.BooleanField(
        pgettext_lazy('novaposhta', "Is dispatched"),
        default=False
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Scan Sheet")
        verbose_name_plural = pgettext_lazy('novaposhta', "Scan Sheets")

    def __str__(self) -> str:
        return self.number or self.ref
