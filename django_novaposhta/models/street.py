from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel, AutocompleteSearchFieldMixin

__all__ = (
    "Street",
    "SettlementStreet",
)


class Street(BaseNovaposhtaModel):
    city = models.ForeignKey(
        'django_novaposhta.City',
        null=True,
        on_delete=models.CASCADE,
        related_name='streets',
        verbose_name=pgettext_lazy('novaposhta', "City")
    )
    streets_type_ref = models.CharField(
        pgettext_lazy('novaposhta', "Streets type ref"),
        max_length=50
    )
    streets_type = models.CharField(
        pgettext_lazy('novaposhta', "Streets type"),
        max_length=50
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Street")
        verbose_name_plural = pgettext_lazy('novaposhta', "Streets")


class SettlementStreet(
    AutocompleteSearchFieldMixin,
    BaseNovaposhtaModel
):
    description = None
    ref = None
    settlement = models.ForeignKey(
        'django_novaposhta.Settlement',
        null=True,
        on_delete=models.CASCADE,
        related_name='settlement_streets',
        verbose_name=pgettext_lazy('novaposhta', "Settlement")
    )
    streets_type_description = models.CharField(
        pgettext_lazy('novaposhta', "Streets type ref"),
        max_length=50
    )
    streets_type = models.CharField(
        pgettext_lazy('novaposhta', "Streets type ref"),
        max_length=50
    )
    settlement_street_ref = models.CharField(
        pgettext_lazy('novaposhta', "Settlement street ref"),
        max_length=50, db_index=True
    )
    settlement_street_description = models.CharField(
        pgettext_lazy('novaposhta', "Settlement street description"),
        max_length=255,
        db_index=True
    )
    settlement_street_description_ru = models.CharField(
        pgettext_lazy('novaposhta', "Settlement street description ru"),
        max_length=255,
        db_index=True
    )
    present = models.CharField(
        pgettext_lazy('novaposhta', "Present"),
        max_length=255,
    )
    location = JSONField(blank=True, default=dict)

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Settlement street")
        verbose_name_plural = pgettext_lazy('novaposhta', "Settlement streets")

    @staticmethod
    def autocomplete_search_fields():
        return 'settlement_street_description',

    @property
    def description(self):
        return self.settlement_street_description

    @property
    def ref(self):
        return self.settlement_street_ref

    @property
    def description_ru(self):
        return self.settlement_street_description_ru

    @property
    def streets_type_ref(self):
        return self.streets_type
