from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel, AutocompleteSearchFieldMixin

__all__ = (
    "Settlement",
)


class Settlement(
    AutocompleteSearchFieldMixin,
    BaseNovaposhtaModel
):
    description_ru = models.CharField(
        pgettext_lazy('novaposhta', "Description in russian"),
        max_length=50,
        db_index=True
    )
    area = models.ForeignKey(
        'django_novaposhta.Area',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='settlements',
        verbose_name=pgettext_lazy('novaposhta', "Area"),
    )
    region = models.ForeignKey(
        'django_novaposhta.Region',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='settlements',
        verbose_name=pgettext_lazy('novaposhta', "Region"),
    )
    settlement_type = models.ForeignKey(
        'django_novaposhta.SettlementType',
        null=True,
        on_delete=models.CASCADE,
        related_name='settlements',
        verbose_name=pgettext_lazy('novaposhta', "Settlement Type"),
    )
    longitude = models.CharField(
        pgettext_lazy('novaposhta', "Longitude"),
        blank=True,
        max_length=50,
    )
    latitude = models.CharField(
        pgettext_lazy('novaposhta', "Latitude"),
        blank=True,
        max_length=50
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Settlement")
        verbose_name_plural = pgettext_lazy('novaposhta', "Settlements")
