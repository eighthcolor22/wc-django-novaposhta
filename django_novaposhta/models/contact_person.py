from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import (
    BaseNovaposhtaModel,
    BasePerson,
    AutocompleteSearchFieldMixin,
)

__all__ = (
    "ContactPerson",
)


class ContactPerson(
    AutocompleteSearchFieldMixin,
    BasePerson,
    BaseNovaposhtaModel
):
    counterparty = models.ForeignKey(
        'django_novaposhta.Counterparty',
        on_delete=models.CASCADE,
        related_name='contact_persons',
        verbose_name=pgettext_lazy('novaposhta', "Counterparty")
    )
    phones = models.CharField(
        pgettext_lazy('novaposhta', "Phones"),
        max_length=255,
        blank=True
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Contact person")
        verbose_name_plural = pgettext_lazy('novaposhta', "Contact persons")
