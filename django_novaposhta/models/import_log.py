from django.db import models
from django.utils.translation import pgettext_lazy
from django.utils.text import Truncator

__all__ = (
    'ImportLog',
)


class ImportLog(models.Model):
    messages = models.TextField(
        pgettext_lazy('novaposhta', "Message"),
        blank=True,
    )
    created_at = models.DateTimeField(
        pgettext_lazy('novaposhta', 'Created at'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        pgettext_lazy('novaposhta', 'Updated at'),
        auto_now=True
    )

    class Meta:
        ordering = ['-created_at']
        verbose_name = pgettext_lazy('novaposhta', "Import log")
        verbose_name_plural = pgettext_lazy('novaposhta', "Import logs")

    def __str__(self) -> str:
        return f'{self.date} --> {self.text}'

    @property
    def date(self):
        return self.created_at.strftime("%Y-%m-%d %H:%M")

    @property
    def text(self):
        return Truncator(self.messages).chars(150)

    def set_massage(self, message: str):
        if not self.messages:
            self.messages = ''

        self.messages += message
        self.save()
