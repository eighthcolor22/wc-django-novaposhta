from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel, AutocompleteSearchFieldMixin

__all__ = (
    "Region",
)


class Region(
    AutocompleteSearchFieldMixin,
    BaseNovaposhtaModel
):
    description_ru = models.CharField(
        pgettext_lazy('novaposhta', "Description in russian"),
        max_length=50,
        db_index=True
    )
    area = models.ForeignKey(
        'django_novaposhta.Area',
        null=True,
        on_delete=models.SET_NULL,
        related_name='regions',
        verbose_name=pgettext_lazy('novaposhta', "Area"),
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Region")
        verbose_name_plural = pgettext_lazy('novaposhta', "Regions")
