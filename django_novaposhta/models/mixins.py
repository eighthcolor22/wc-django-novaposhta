from django.contrib.contenttypes.models import ContentType

__all__ = (
    'ContentTypeGetterMixin',
)


class ContentTypeGetterMixin:
    @property
    def _type(self):
        """
        Return content type's model
        """
        ct = self.get_content_type()
        return '.'.join(ct.natural_key())

    @classmethod
    def get_content_type(cls):
        return ContentType.objects.get_for_model(cls)
