from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import (
    AutocompleteSearchFieldMixin,
    BaseNovaposhtaModel,
    BasePerson,
)

__all__ = (
    "Counterparty",
)


class Counterparty(
    AutocompleteSearchFieldMixin,
    BasePerson,
    BaseNovaposhtaModel
):
    user = models.ForeignKey(
        get_user_model(),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='counterparties',
        verbose_name=pgettext_lazy('novaposhta', "User")
    )
    phone = models.CharField(
        pgettext_lazy('novaposhta', "Phone"),
        max_length=255,
        blank=True
    )
    city = models.ForeignKey(
        'django_novaposhta.City',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='counterparties',
        verbose_name=pgettext_lazy('novaposhta', "City"),
    )
    counterparty = models.CharField(
        pgettext_lazy('novaposhta', "Counterparty"),
        max_length=255,
        blank=True
    )
    counterparty_type = models.ForeignKey(
        'django_novaposhta.CounterpartyType',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='counterparties',
        verbose_name=pgettext_lazy('novaposhta', "Counterparty type")
    )
    counterparty_property = models.ForeignKey(
        'django_novaposhta.PayerType',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='counterparties',
        verbose_name=pgettext_lazy('novaposhta', "Counterparty property")
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Counterparty")
        verbose_name_plural = pgettext_lazy('novaposhta', "Counterparties")
