from django.conf import settings
from django.contrib import admin
from django.utils.translation import pgettext_lazy

from .forms import AddressAdminForm
from ..models import (
    Address,
    Area,
    City,
    ContactPerson,
    Counterparty,
    Document,
    ImportLog,
    Region,
    ScanSheet,
    Settlement,
    Street,
    SettlementStreet,
    Warehouse,
)
from ..services import (
    get_document_pdf,
    delete_document,
    check_documents_status,
    populate_warehouses_with_settlement_ref2,
)


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    form = AddressAdminForm
    list_display = (
        "__str__",
        "ref",
        'city',
    )
    search_fields = (
        "ref",
    )
    list_select_related = (
        'street',
        'street__city',
    )

    def city(self, obj):
        return obj.street.city.description


admin.site.register(ImportLog)
admin.site.register(
    Area,
    list_display=(
        "ref",
        "description",
        "areas_center",
        "is_active"
    ),
    list_select_related=(
        "areas_center",
    ),
    readonly_fields=(
        'created_at',
        'updated_at'
    ),
    search_fields=(
        "ref",
        "description",
    ),
)
admin.site.register(
    City,
    list_display=(
        "description",
        "area",
        "settlement_type"
    ),
    list_filter=(
        "area",
        "settlement_type",
    ),
    list_select_related=(
        "area",
        "settlement_type"
    ),
    readonly_fields=(
        'created_at',
        'updated_at'
    ),
    search_fields=(
        "ref",
        "description",
        "description_ru",
    ),
)
admin.site.register(
    Settlement,
    list_display=(
        "description",
        "area",
        "region",
        "settlement_type"
    ),
    list_filter=(
        "area",
        "settlement_type",
    ),
    list_select_related=(
        "area",
        "region",
        "settlement_type"
    ),
    readonly_fields=(
        'created_at',
        'updated_at'
    ),
    search_fields=(
        "ref",
        "description",
        "description_ru",
    ),
)
admin.site.register(
    Region,
    list_display=(
        "description",
        "area"
    ),
    list_select_related=(
        "area",
    ),
    readonly_fields=(
        'created_at',
        'updated_at'
    ),
    search_fields=(
        "ref",
        "description",
    ),
)


def populate_warehouse_settlement_ref(modeladmin, request, queryset):
    for warehouse in queryset:
        settlements = (
            Settlement.objects
            .filter(description=warehouse.city.description)
            .distinct()
        )

        for settlement in settlements:
            populate_warehouses_with_settlement_ref2(
                settlement_ref=settlement.ref
            )


admin.site.register(
    Warehouse,
    actions=[
        populate_warehouse_settlement_ref,
    ],
    list_display=(
        "city",
        "description",
        "number",
        "phone",
        "type_of_warehouse",
        'settlement_ref'
    ),
    list_filter=(
        "city",
        "type_of_warehouse",
    ),
    list_select_related=(
        "city",
        "type_of_warehouse",
    ),
    readonly_fields=(
        'created_at',
        'updated_at'
    ),
    ordering=(
        "-city",
        'description'
    ),
    search_fields=(
        "ref",
        "description",
        "description_ru",
        "city_ref",
        "city__description",
        "city__description_ru",
    ),
)
admin.site.register(
    Street,
    list_display=(
        "city",
        "description",
        "streets_type",
    ),
    list_filter=(
        "city",
        "city__area",
        "streets_type",
    ),
    list_select_related=(
        "city",
    ),
    ordering=(
        "-city",
        'description'
    ),
    search_fields=(
        "ref",
        "description",
        "city__description",
        "city__description_ru",
    ),
)

admin.site.register(
    SettlementStreet,
    list_display=(
        "settlement",
        "description",
        "streets_type_description",
    ),
    list_filter=(
        "settlement",
    ),
    list_select_related=(
        "settlement",
    ),
    ordering=(
        "-settlement",
        'settlement_street_description'
    ),
    search_fields=(
        "settlement_street_ref",
        "settlement__description",
        "settlement__description_ru",
    ),
)


def get_documents(modeladmin, request, queryset):
    for obj in queryset:
        get_document_pdf(document=obj)


get_documents.short_description = pgettext_lazy('novaposhta', "Get documents")


def delete_documents(modeladmin, request, queryset):
    for obj in queryset:
        delete_document(document=obj)


delete_documents.short_description = pgettext_lazy('novaposhta', "Delete documents")


def check_statuses(modeladmin, request, queryset):
    check_documents_status(documents=queryset)


check_statuses.short_description = pgettext_lazy('novaposhta', "Check statuses")


admin.site.register(
    Document,
    actions=(
        get_documents,
        delete_documents,
        check_statuses
    ),
    list_display=(
        "int_doc_number",
        "is_tracking",
        "status",
        "scan_sheet",
    ),
    list_filter=(
        "status",
        "is_tracking"
    ),
    list_select_related=(
        "scan_sheet",
    ),
    readonly_fields=(
        'created_at',
        'updated_at'
    ),
    search_fields=(
        "ref",
        "int_doc_number",
    ),
)


class DocumentAdminBaseInline:
    model = Document
    readonly_fields = (
        'created_at',
        'updated_at'
    )
    extra = 0


if 'jet' in settings.INSTALLED_APPS:
    from jet.admin import CompactInline

    document_inline_inheritance_class = CompactInline
else:
    document_inline_inheritance_class = admin.StackedInline


class DocumentAdminBaseInline(DocumentAdminBaseInline, document_inline_inheritance_class):
    pass


admin.site.register(
    ScanSheet,
    inlines=[DocumentAdminBaseInline],
    list_display=(
        "ref",
        "number",
        "date"
    ),
    readonly_fields=(
        'created_at',
        'updated_at'
    ),
    search_fields=(
        "ref",
        "number",
    ),
)


@admin.register(ContactPerson)
class ContactPersonAdmin(admin.ModelAdmin):
    list_display = (
        'description',
        'counterparty',
    )
    list_select_related = (
        'counterparty',
    )
    readonly_fields = (
        'created_at',
        'updated_at'
    )
    search_fields = (
        'ref',
        'description',
        'first_name',
        'middle_name',
        'last_name',
        'phones',
        'email'
    )


@admin.register(Counterparty)
class CounterpartyAdmin(admin.ModelAdmin):
    list_display = (
        'description',
        'user',
        'ref',
        'counterparty_type',
    )
    list_select_related = (
        'user',
        'counterparty_type',
    )
    readonly_fields = (
        'created_at',
        'updated_at'
    )
    search_fields = (
        'ref',
        'description',
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'email'
    )
