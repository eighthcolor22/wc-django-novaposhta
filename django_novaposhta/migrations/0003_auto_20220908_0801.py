# Generated by Django 3.0.6 on 2022-09-08 08:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('django_novaposhta', '0002_auto_20220905_0830'),
    ]

    operations = [
        migrations.AddField(
            model_name='userpreferences',
            name='cargo_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='django_novaposhta.CargoType', verbose_name='Default cargo type'),
        ),
        migrations.AddField(
            model_name='userpreferences',
            name='payer_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='django_novaposhta.PayerType', verbose_name='Default payer type'),
        ),
    ]
