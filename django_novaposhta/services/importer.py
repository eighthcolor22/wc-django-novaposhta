from typing import Dict, List, TYPE_CHECKING, Type

from django.utils.translation import pgettext_lazy

from ..exceptions import NovaposhtaResponseError
from ..models import (
    Area,
    BackwardDeliveryCargoType,
    CargoType,
    City,
    CounterpartyType,
    ImportLog,
    PayerType,
    PaymentForm,
    ServiceType,
    Settlement,
    Street,
    Warehouse,
    WarehouseType,
)
from ..sdk import (
    get_address_client,
    get_common_client
)

if TYPE_CHECKING:
    from ..sdk import BaseServiceAdapter
    from ..entities.base import BaseImportEntity
    from ..models.abstracts import BaseNovaposhtaModel

__all__ = (
    'import_areas',
    'import_cities',
    'import_settlements',
    'import_warehouse_types',
    'import_warehouses',
    'import_streets',
    'import_payer_types',
    'import_payment_forms',
    'import_cargo_types',
    'import_backward_delivery_cargo_types',
    'import_service_types',
    'import_counterparty_types',
    'populate_warehouses_with_settlement_ref',
    'populate_warehouses_with_settlement_ref2'
)


def _update_object(data: Dict, instance: 'BaseNovaposhtaModel'):
    is_changed = False

    for object_field, value in data.items():
        object_value = getattr(instance, object_field, None)

        if object_value != value:
            setattr(instance, object_field, value)
            is_changed = True

    if is_changed:
        instance.save()

    return instance


# @transaction.atomic()
def _import_catalog_instances(
        *,
        novaposhta_model: Type['BaseNovaposhtaModel'],
        client: 'BaseServiceAdapter',
        caller: str,
        import_log: 'ImportLog' = None,
        **kwargs
):
    if import_log is None:
        import_log = ImportLog(messages="")

    delete_filter = kwargs.pop('delete_filter', {})
    processed_ids = set()
    func = getattr(client, caller)
    items: List['BaseImportEntity'] = func(**kwargs)
    success_import = True

    while True:
        try:
            item = next(items)
            print('-----')
            print(item)
            data: Dict = item.as_dict()
            instance = (
                novaposhta_model
                .objects
                .filter(ref=data['ref'])
                .first()
            )

            if not instance:
                instance = novaposhta_model()

            instance = _update_object(
                data=data,
                instance=instance
            )

            processed_ids.add(instance.pk)
        except StopIteration as e:
            break
        except NovaposhtaResponseError as e:
            import_log.set_massage(str(e) + "\n")
            success_import = False
            break
        except Exception as e:
            import_log.set_massage(str(e) + "\n")

    if processed_ids and success_import:
        (
            novaposhta_model.objects
            .filter(**delete_filter)
            .exclude(pk__in=processed_ids)
            .delete()
        )

    import_log.set_massage(
        str(
            pgettext_lazy(
                'novaposhta',
                f"{novaposhta_model.__name__}: "
                f"Processed {len(processed_ids)} record.\n"
                f"Params: {delete_filter}.\n\n"
            )
        )
    )


def import_areas():
    _import_catalog_instances(
        novaposhta_model=Area,
        client=get_address_client(),
        caller='get_areas',
        delete_filter={
            'is_active': True
        },
    )


def import_cities():
    _import_catalog_instances(
        novaposhta_model=City,
        client=get_address_client(),
        caller='get_cities',
    )


def import_settlements():
    _import_catalog_instances(
        novaposhta_model=Settlement,
        client=get_address_client(),
        caller='get_settlements',
    )


def import_warehouse_types():
    _import_catalog_instances(
        novaposhta_model=WarehouseType,
        client=get_address_client(),
        caller='get_warehouse_types',
    )


def import_warehouses():
    _import_catalog_instances(
        novaposhta_model=Warehouse,
        client=get_address_client(),
        caller='get_warehouses',
    )


def import_streets():
    import_log = ImportLog(messages="")

    for city in City.objects.all():
        _import_catalog_instances(
            novaposhta_model=Street,
            client=get_address_client(),
            caller='get_streets',
            import_log=import_log,
            delete_filter={
                'city': city
            },
            city_ref=city.ref,
        )


def import_payer_types():
    _import_catalog_instances(
        novaposhta_model=PayerType,
        client=get_common_client(),
        caller='get_payer_types',
    )


def import_payment_forms():
    _import_catalog_instances(
        novaposhta_model=PaymentForm,
        client=get_common_client(),
        caller='get_payment_forms',
    )


def import_cargo_types():
    _import_catalog_instances(
        novaposhta_model=CargoType,
        client=get_common_client(),
        caller='get_cargo_types'
    )


def import_backward_delivery_cargo_types():
    _import_catalog_instances(
        novaposhta_model=BackwardDeliveryCargoType,
        client=get_common_client(),
        caller='get_backward_delivery_cargo_types'
    )


def import_service_types():
    _import_catalog_instances(
        novaposhta_model=ServiceType,
        client=get_common_client(),
        caller='get_service_types'
    )


def import_counterparty_types():
    _import_catalog_instances(
        novaposhta_model=CounterpartyType,
        client=get_common_client(),
        caller='get_counterparties_types'
    )


def populate_warehouses_with_settlement_ref(batch_size: int = 50):
    client = get_address_client()
    db_warehouses = Warehouse.objects.in_bulk(field_name='ref')
    to_update_ = []

    def _update(to_update: List):
        Warehouse.objects.bulk_update(
            to_update,
            fields=['settlement_ref'],
            batch_size=batch_size
        )
        to_update = []

    for settlement in Settlement.objects.all():
        warehouses = client.get_warehouses(
            properties={'SettlementRef': settlement.ref}
        )

        for warehouse in warehouses:
            print(settlement, warehouse)
            db_warehouse = db_warehouses.get(warehouse.ref)

            if db_warehouse:
                db_warehouse.settlement_ref = settlement.ref
                to_update_.append(db_warehouse)

            if len(to_update_) >= batch_size:
                _update(to_update_)

        # time.sleep(1)

    _update(to_update_)


def populate_warehouses_with_settlement_ref2(settlement_ref: str, batch_size: int = 50):
    def _update(to_update: List):
        Warehouse.objects.bulk_update(
            to_update,
            fields=['settlement_ref'],
            batch_size=batch_size
        )
        to_update = []

    to_update_ = []
    client = get_address_client()
    warehouses = client.get_warehouses(
        properties={'SettlementRef': settlement_ref}
    )

    warehouses_refs = [warehouse.ref for warehouse in warehouses]

    try:
        db_warehouses = (
            Warehouse.objects
            .filter(
                ref__in=warehouses_refs
            )
            .in_bulk(
                field_name='ref'
            )
        )
    except:
        return

    for warehouse_ref in warehouses_refs:
        db_warehouse = db_warehouses.get(warehouse_ref)

        if db_warehouse:
            db_warehouse.settlement_ref = settlement_ref
            to_update_.append(db_warehouse)

        if len(to_update_) >= batch_size:
            _update(to_update_)

    _update(to_update_)
