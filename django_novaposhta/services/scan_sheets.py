from datetime import datetime
from typing import Iterable, Iterator, List, TYPE_CHECKING, Union, Optional

import pytz
from django.utils import timezone

from ..consts import NOVAPOSHTA_TIME_ZONE
from ..models import (
    ScanSheet as ScanSheetModel,
    Document as DocumentModel,
)
from ..sdk import (
    get_scan_sheet_client,
    get_prints_client
)
from ..utils import create_temporary_file_from_url

if TYPE_CHECKING:
    from ..entities import ScanSheet
    from ..models import (
        Preferences,
        UserPreferences,
    )


__all__ = (
    'insert_documents',
    'expand_scan_sheet',
    'get_scan_sheet',
    'get_scan_sheet_list',
    'delete_scan_sheet',
    'delete_scan_sheets',
    'remove_documents',
    'get_scan_sheet_pdf',
    'get_scan_sheet_documents_markings_zebra_pdf'
)


def insert_documents(
        *,
        documents: List['DocumentModel'],
        save_scan_sheet_pdf: bool = False,
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> 'ScanSheetModel':
    client = get_scan_sheet_client(preferences=preferences)

    refs: List[str] = [document.ref for document in documents]

    scan_sheet_entity: 'ScanSheet' = (
        client.insert_documents(refs)
    )

    data = scan_sheet_entity.as_dict()
    date = (
        datetime.strptime(
            data['date'],
            '%Y-%m-%d %H:%M:%S',

        )
    )
    date = timezone.make_aware(date, timezone=pytz.timezone(NOVAPOSHTA_TIME_ZONE))
    data['date'] = date

    scan_sheet: 'ScanSheetModel' = (
        ScanSheetModel.objects.create(**data)
    )

    if save_scan_sheet_pdf:
        get_scan_sheet_pdf(scan_sheet=scan_sheet)

    for document in documents:
        document.scan_sheet = scan_sheet
        document.save(update_fields=['scan_sheet'])

    return scan_sheet


def expand_scan_sheet(
        *,
        documents: List['DocumentModel'],
        scan_sheet: 'ScanSheetModel' = None,
        save_scan_sheet_pdf: bool = False,
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> 'ScanSheetModel':
    client = get_scan_sheet_client(preferences=preferences)

    refs: List[str] = [document.ref for document in documents]

    scan_sheet_ref = scan_sheet.ref
    scan_sheet_entity: 'ScanSheet' = (
        client.expand(
            document_refs=refs,
            scan_sheet_ref=scan_sheet_ref
        )
    )

    data = scan_sheet_entity.as_dict()
    data['date'] = (
        datetime.strptime(
            data['date'],
            '%Y-%m-%d %H:%M:%S'
        )
    )

    if save_scan_sheet_pdf:
        get_scan_sheet_pdf(scan_sheet=scan_sheet)

    for document in documents:
        document.scan_sheet = scan_sheet
        document.save(update_fields=['scan_sheet'])

    return scan_sheet


def get_scan_sheet(
        *,
        scan_sheet: 'ScanSheetModel',
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> 'ScanSheetModel':
    client = get_scan_sheet_client(preferences=preferences)

    scan_sheet_entity: Union['ScanSheet', dict] = (
        client.get(ref=scan_sheet.ref)
    )
    if isinstance(scan_sheet_entity, dict) and not scan_sheet_entity:
        # scan_sheet is empty case
        scan_sheet.count = 0
        scan_sheet.save()
        scan_sheet.documents.all().update(scan_sheet=None)
        return scan_sheet

    data = scan_sheet_entity.as_dict()
    scan_sheet.count = data['count']
    scan_sheet.city_sender = data['city_sender']
    scan_sheet.sender_address = data['sender_address']
    scan_sheet.sender = data['sender']

    if scan_sheet_entity.raw_data:
        scan_sheet.raw_data = scan_sheet_entity.raw_data

    scan_sheet.save()

    return scan_sheet


def get_scan_sheet_list(
        *,
        with_update: bool = False,
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> List['ScanSheetModel']:
    client = get_scan_sheet_client(preferences=preferences)

    response: Iterator['ScanSheet'] = client.get_list()

    scan_sheets = (
        ScanSheetModel.objects.filter(
            ref__in=[item.ref for item in response]
        )
    )

    if with_update:
        scan_sheets_map = {
            scan_sheet.ref: scan_sheet
            for scan_sheet in scan_sheets
        }

        for scan_sheet_entity in response:
            scan_sheet: 'ScanSheetModel' = (
                scan_sheets_map.get(scan_sheet_entity.ref)
            )

            if scan_sheet:
                scan_sheet.printed = scan_sheet_entity.printed
                scan_sheet.save(update_fields=['printed'])

    return scan_sheets


def delete_scan_sheets(
        *,
        scan_sheets: Iterable['ScanSheetModel'],
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> None:
    client = get_scan_sheet_client(preferences=preferences)

    scan_sheet_refs = [
        scan_sheet.ref for scan_sheet in scan_sheets
    ]
    response_data = client.delete(refs=scan_sheet_refs)
    deleted_scan_sheets_refs = [
        item['Ref'] for item in
        response_data.get('ScanSheetRefs', {}).get('Success', [])
    ]
    if deleted_scan_sheets_refs:
        ScanSheetModel.objects.filter(ref__in=deleted_scan_sheets_refs).delete()


def delete_scan_sheet(
        *,
        scan_sheet: 'ScanSheetModel',
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> None:
    delete_scan_sheets(
        scan_sheets=[scan_sheet],
        preferences=preferences
    )


def remove_documents(
        *,
        documents: Iterable['DocumentModel'],
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> None:
    client = get_scan_sheet_client(preferences=preferences)

    document_refs = [
        document.ref for document in documents
    ]

    response_data = client.remove_documents(document_refs=document_refs)
    removed_documents_refs = [
        item['Ref'] for item in
        response_data.get('DocumentRefs', {}).get('Success', [])
    ]
    if removed_documents_refs:
        (
            DocumentModel
            .objects
            .filter(ref__in=removed_documents_refs)
            .update(scan_sheet=None)
        )


def get_scan_sheet_pdf(
        *,
        scan_sheet: 'ScanSheetModel',
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> None:
    client = get_prints_client(preferences=preferences)
    url = (
        client.get_scan_sheet_url(
            ref=scan_sheet.ref,
        )
    )
    file = create_temporary_file_from_url(url=url)

    scan_sheet.pdf.save(
        f"scan_sheet_{scan_sheet.number or scan_sheet.ref}.pdf",
        file
    )
    scan_sheet.printed = 1
    scan_sheet.save()


def get_scan_sheet_documents_markings_zebra_pdf(
        *,
        scan_sheet: 'ScanSheetModel',
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> None:
    client = get_prints_client(preferences=preferences)
    documents = scan_sheet.documents.all()
    numbers = [document.ref for document in documents]

    url = (
        client.get_markings_url(
            numbers=numbers,
            is_zebra_printer=True
        )
    )
    file = create_temporary_file_from_url(url=url)

    scan_sheet.marking_pdf.save(
        f"scan_sheet_{scan_sheet.number or scan_sheet.ref}_zebra_print.pdf",
        file
    )
    scan_sheet.save()
