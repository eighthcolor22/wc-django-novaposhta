from datetime import datetime
from typing import TYPE_CHECKING, Iterable, Iterator, Union, List

from django.core.paginator import Paginator
from django.utils.timezone import now

from ..consts import (
    DOCUMENT_TRACKING_DISABLE_STATUSES,
    DEFAULT_PAYMENT_METHOD,
    DEFAULT_SERVICE_TYPE,
    DEFAULT_PAYER_TYPE,
    DEFAULT_CARGO_TYPE,
    DEFAULT_BACKWARD_DELIVERY_CARGO_TYPE,
    DEFAULT_DOCUMENT_WEIGHT,
    DOCUMENT_TRACKING_DELETED_STATUS,
    DOCUMENT_TRACKING_DELETED_VERBOSE_STATUS,
    DEFAULT_DESCRIPTION,
    NOVAPOSHTA_MINIMAL_ESTIMATED_COST,
)
from ..entities import CatalogItem, Document, DocumentStatus, BackwardDeliveryData
from ..models import (
    Preferences,
    Document as DocumentModel,
)
from ..getters import get_privat_person_counterparty_type_obj
from ..sdk import (
    get_document_client,
    get_tracking_document_client,
    get_prints_client
)
from ..utils import create_temporary_file_from_url

if TYPE_CHECKING:
    from ..entities import (
        DocumentPrice,
        DocumentDeliveryDate
    )
    from ..models import (
        Address,
        BackwardDeliveryCargoType,
        CargoType,
        City,
        ContactPerson,
        Counterparty,
        PayerType,
        PaymentForm,
        ServiceType,
        UserPreferences,
        Settlement,
        CounterpartyType,
    )

__all__ = (
    'create_document',
    'create_document_address_as_string',
    'update_document',
    'update_document_address_as_string',
    'check_documents_status',
    'delete_document',
    'delete_documents',
    'get_document_price',
    'get_document_delivery_date',
    'get_document_pdf',
    'get_marking_pdf',
    'track_actual_documents',
)


def _prepare_document_data(
        *,
        preferences: Union['Preferences', 'UserPreferences'],
        option_seat: List[dict],
        payer_type: 'PayerType' = None,
        payment_form: 'PaymentForm' = None,
        cargo_type: 'CargoType' = None,
        service_type: 'ServiceType' = None,
        description: str = None,
        cost: int = None,
        sender_city: 'str' = None,
        sender: 'str' = None,
        sender_address: 'str' = None,
        sender_phone: 'str' = None,
        sender_contact: 'str' = None,
        backward_delivery_data: Union[List[BackwardDeliveryData], List[dict]] = None,
) -> dict:
    global_preferences = Preferences.get_solo()
    payer_type = (
        payer_type or preferences.payer_type or
        global_preferences.payer_type
    )

    payment_form = (
        payment_form or preferences.payment_form or
        global_preferences.payment_form
    )

    cargo_type = (
        cargo_type or preferences.cargo_type or
        global_preferences.cargo_type
    )

    service_type = (
        service_type or preferences.service_type or
        global_preferences.service_type
    )

    seats_amount = len(option_seat)
    document_data = {
        "PayerType": (
            payer_type.ref
            if payer_type else DEFAULT_PAYER_TYPE
        ),
        "PaymentMethod": (
            payment_form.ref
            if payment_form else DEFAULT_PAYMENT_METHOD
        ),
        "DateTime": now().strftime("%d.%m.%Y"),
        "CargoType": (
            cargo_type.ref
            if cargo_type else DEFAULT_CARGO_TYPE
        ),
        "ServiceType": (
            service_type.ref
            if service_type else DEFAULT_SERVICE_TYPE
        ),
        "SeatsAmount": str(seats_amount),
        "Description": (
            description or
            global_preferences and description or
            DEFAULT_DESCRIPTION
        ),
        "Cost": (
            cost or
            global_preferences and global_preferences.cost or
            NOVAPOSHTA_MINIMAL_ESTIMATED_COST
        ),
        "CitySender": sender_city or preferences.sender_city,
        "Sender": sender or preferences.sender,
        "SenderAddress": sender_address or preferences.sender_address,
        "ContactSender": sender_contact or preferences.sender_contact,
        "SendersPhone": sender_phone or preferences.sender_phone,

    }

    option_seat = list(
            map(
                lambda item: {key: float(val) for key, val in item.items()},
                option_seat
            )
        )
    document_data.update(
            {"OptionsSeat": option_seat}
        )

    if backward_delivery_data:
        document_data['BackwardDeliveryData'] = list(
            map(
                lambda item: (
                    item.as_data if isinstance(item, BackwardDeliveryData) else item
                ),
                backward_delivery_data
            )
        )

    return document_data


def create_document(
        *,
        preferences: Union['Preferences', 'UserPreferences'],
        recipient_city: Union['City', 'Settlement'],
        recipient: 'Counterparty',
        recipient_contact: 'ContactPerson',
        recipient_phone: str,
        option_seat: List[dict],
        recipient_address: Union['str', 'Address'],
        recipient_type: 'CounterpartyType' = None,
        payer_type: 'PayerType' = None,
        payment_form: 'PaymentForm' = None,
        cargo_type: 'CargoType' = None,
        service_type: 'ServiceType' = None,
        description: str = None,
        cost: int = None,
        sender_city: 'str' = None,
        sender: 'str' = None,
        sender_address: 'str' = None,
        sender_phone: 'str' = None,
        sender_contact: 'str' = None,
        backward_delivery_data: Union[List[BackwardDeliveryData], List[dict]] = None,
        should_get_pdf: bool = False,
        **kwargs
) -> 'DocumentModel':
    """
    Common variant to create document. Sender address is Address instance or string ref.
    It should be used when you store address data locally.
    DOCS: https://devcenter.novaposhta.ua/docs/services/556eef34a0fe4f02049c664e/operations/556ef753a0fe4f02049c664f
    Note: Recipient address can be got only with Street instance. SettlementStreet instance will raise error.
    To create novaposhta document in the the best known way, this func should take next arguments:
        - option_seat: List[Dict]. See docs:
            https://devcenter.novaposhta.ua/docs/services/556eef34a0fe4f02049c664e/operations/57484280a0fe4f33f0d4dd77
        - seats_amount len(option_seat)
    """
    client = get_document_client(preferences=preferences)

    document_data = {
        **_prepare_document_data(
            preferences=preferences, payer_type=payer_type,
            payment_form=payment_form, cargo_type=cargo_type,
            service_type=service_type, description=description, cost=cost,
            option_seat=option_seat, sender_city=sender_city,
            sender=sender, sender_address=sender_address,
            sender_phone=sender_phone, sender_contact=sender_contact,
            backward_delivery_data=backward_delivery_data
        ),
        "CityRecipient": recipient_city.ref,
        "Recipient": recipient.ref,
        "RecipientAddress": (
            recipient_address if isinstance(recipient_address, str)
            else recipient_address.ref
        ),
        'ContactRecipient': recipient_contact.ref,
        "RecipientsPhone": recipient_phone,
        "RecipientType": (
             recipient_type and recipient_type.ref or
             get_privat_person_counterparty_type_obj().ref
        ),
        **kwargs
    }

    document_entity: 'Document' = client.save(document_data)

    data = document_entity.as_dict()
    data['request_data'] = document_data
    document: 'DocumentModel' = (
        DocumentModel.objects.create(**data)
    )

    if should_get_pdf:
        get_document_pdf(
            document=document,
            preferences=preferences
        )
    return document


def update_document(
        *,
        document: 'DocumentModel',
        preferences: Union['Preferences', 'UserPreferences'],
        recipient_city: Union['City', 'Settlement'],
        recipient: 'Counterparty',
        recipient_contact: 'ContactPerson',
        recipient_phone: str,
        option_seat: List[dict],
        recipient_address: Union['str', 'Address'],
        recipient_type: 'CounterpartyType' = None,
        payer_type: 'PayerType' = None,
        payment_form: 'PaymentForm' = None,
        cargo_type: 'CargoType' = None,
        service_type: 'ServiceType' = None,
        description: str = None,
        cost: int = None,
        sender_city: 'str' = None,
        sender: 'str' = None,
        sender_address: 'str' = None,
        sender_phone: 'str' = None,
        sender_contact: 'str' = None,
        should_get_pdf: bool = False,
        backward_delivery_data: Union[List[BackwardDeliveryData], List[dict]] = None,
        **kwargs
) -> 'DocumentModel':
    client = get_document_client(preferences=preferences)

    document_data = {
        **_prepare_document_data(
            preferences=preferences, payer_type=payer_type,
            payment_form=payment_form, cargo_type=cargo_type,
            service_type=service_type, description=description, cost=cost,
            option_seat=option_seat, sender_city=sender_city,
            sender=sender, sender_address=sender_address,
            sender_phone=sender_phone, sender_contact=sender_contact,
            backward_delivery_data=backward_delivery_data
        ),
        "CityRecipient": recipient_city.ref,
        "Recipient": recipient.ref,
        "RecipientAddress": (
            recipient_address if isinstance(recipient_address, str)
            else recipient_address.ref
        ),
        'ContactRecipient': recipient_contact.ref,
        "RecipientsPhone": recipient_phone,
        "RecipientType": (
                recipient_type or
                get_privat_person_counterparty_type_obj().ref
        ),
        "Ref": document.ref,
        **kwargs
    }

    document_entity: 'Document' = client.update(document_data)

    data = document_entity.as_dict()
    data['request_data'] = document_data
    document, updated = (
        DocumentModel.objects.update_or_create(
            ref=document_entity.ref,
            defaults={
                **data,
            }
        )
    )

    if should_get_pdf:
        get_document_pdf(
            document=document,
            preferences=preferences
        )
    return document


def create_document_address_as_string(
    *,
    preferences: Union['Preferences', 'UserPreferences'],
    recipient_city_name: str,
    recipient_area: str,
    recipient_area_regions: str,
    recipient_address_name: str,
    settlement_type: str,
    recipient_name: str,
    recipient_phone: str,
    recipient_house: str = '',
    recipient_flat: str = '',
    option_seat: List[dict],
    recipient_type: 'CounterpartyType' = None,
    payer_type: 'PayerType' = None,
    payment_form: 'PaymentForm' = None,
    cargo_type: 'CargoType' = None,
    service_type: 'ServiceType' = None,
    description: str = None,
    cost: int = None,
    sender_city: 'str' = None,
    sender: 'str' = None,
    sender_address: 'str' = None,
    sender_phone: 'str' = None,
    sender_contact: 'str' = None,
    backward_delivery_data: Union[List[BackwardDeliveryData], List[dict]] = None,
    should_get_pdf: bool = False,
    **kwargs
):
    """
    Allow create address delivery document where recipient and recipient address are strings.
    It should be used when you don`t store address data locally.
    DOCS: https://devcenter.novaposhta.ua/docs/services/556eef34a0fe4f02049c664e/operations/56260771a0fe4f1e503fe186
    To create novaposhta document in the the best known way, this func should take next arguments:
        - option_seat: List[Dict]. See docs:
            https://devcenter.novaposhta.ua/docs/services/556eef34a0fe4f02049c664e/operations/57484280a0fe4f33f0d4dd77
        - seats_amount len(option_seat)
    """
    client = get_document_client(preferences=preferences)
    document_data = {
        **_prepare_document_data(
            preferences=preferences, payer_type=payer_type,
            payment_form=payment_form, cargo_type=cargo_type,
            service_type=service_type, description=description, cost=cost,
            option_seat=option_seat, sender_city=sender_city,
            sender=sender, sender_address=sender_address,
            sender_phone=sender_phone, sender_contact=sender_contact,
            backward_delivery_data=backward_delivery_data
        ),
        "NewAddress": "1",
        "RecipientCityName": recipient_city_name,
        "RecipientArea": recipient_area,
        "RecipientAreaRegions": recipient_area_regions,
        "RecipientAddressName": recipient_address_name,
        "RecipientHouse": recipient_house,
        "RecipientFlat": recipient_flat,
        "SettlementType": settlement_type,
        "RecipientName": recipient_name,
        "RecipientsPhone": recipient_phone,
        "RecipientType": (
            recipient_type or
            get_privat_person_counterparty_type_obj().ref
        )
    }
    document_entity: 'Document' = client.save(document_data)

    data = document_entity.as_dict()
    data['request_data'] = document_data
    document: 'DocumentModel' = (
        DocumentModel.objects.create(**data)
    )

    if should_get_pdf:
        get_document_pdf(
            document=document,
            preferences=preferences
        )
    return document


def update_document_address_as_string(
        *,
        document: 'DocumentModel',
        preferences: Union['Preferences', 'UserPreferences'],
        recipient_city_name: str,
        recipient_area: str,
        recipient_area_regions: str,
        recipient_address_name: str,
        settlement_type: str,
        recipient_name: str,
        recipient_phone: str,
        recipient_house: str = '',
        recipient_flat: str = '',
        option_seat: List[dict],
        recipient_type: 'CounterpartyType' = None,
        payer_type: 'PayerType' = None,
        payment_form: 'PaymentForm' = None,
        cargo_type: 'CargoType' = None,
        service_type: 'ServiceType' = None,
        description: str = None,
        cost: int = None,
        sender_city: 'str' = None,
        sender: 'str' = None,
        sender_address: 'str' = None,
        sender_phone: 'str' = None,
        sender_contact: 'str' = None,
        backward_delivery_data: Union[List[BackwardDeliveryData], List[dict]] = None,
        should_get_pdf: bool = False,
        **kwargs
) -> 'DocumentModel':
    client = get_document_client(preferences=preferences)
    document_data = {
        **_prepare_document_data(
            preferences=preferences, payer_type=payer_type,
            payment_form=payment_form, cargo_type=cargo_type,
            service_type=service_type, description=description, cost=cost,
            option_seat=option_seat, sender_city=sender_city,
            sender=sender, sender_address=sender_address,
            sender_phone=sender_phone, sender_contact=sender_contact,
            backward_delivery_data=backward_delivery_data
        ),
        "NewAddress": "1",
        "RecipientCityName": recipient_city_name,
        "RecipientArea": recipient_area,
        "RecipientAreaRegions": recipient_area_regions,
        "RecipientAddressName": recipient_address_name,
        "RecipientHouse": recipient_house,
        "RecipientFlat": recipient_flat,
        "SettlementType": settlement_type,
        "RecipientName": recipient_name,
        "RecipientsPhone": recipient_phone,
        "RecipientType": (
                recipient_type or
                get_privat_person_counterparty_type_obj().ref
        ),
        "Ref": document.ref
    }

    document_entity: 'Document' = client.update(document_data)

    data = document_entity.as_dict()
    data['request_data'] = document_data
    document, updated = (
        DocumentModel.objects.update_or_create(
            ref=document_entity.ref,
            defaults={
                **data,
            }
        )
    )

    if should_get_pdf:
        get_document_pdf(
            document=document,
            preferences=preferences
        )
    return document


def check_documents_status(
    *,
    documents: Iterable['DocumentModel'],
    preferences: 'UserPreferences' = None
):
    client = get_tracking_document_client(preferences=preferences)

    numbers = [document.int_doc_number for document in documents]

    if len(numbers) < 1:
        return

    items: Iterator['DocumentStatus'] = (
        client.track_document_status(numbers)
    )
    # Get statuses, 100 invoices by iteration
    for item in items:
        document = (
            DocumentModel.objects
            .get(int_doc_number=item.number)
        )
        # Will update status, if need
        new_status = item.status_code

        if new_status != document.status or not document.verbose_status:
            if new_status in DOCUMENT_TRACKING_DISABLE_STATUSES:
                document.is_tracking = False
            document.status = new_status
            document.verbose_status = item.status
            document.save()


def delete_documents(
        *,
        documents: Iterable['DocumentModel'],
        user_preferences: 'UserPreferences' = None
) -> List[str]:
    client = get_document_client(preferences=user_preferences)

    document_refs = [document.ref for document in documents]
    document_items: Iterator['CatalogItem'] = client.delete(refs=document_refs)
    document_refs = [item.ref for item in document_items]
    DocumentModel.objects.filter(ref__in=document_refs).update(
        status=DOCUMENT_TRACKING_DELETED_STATUS,
        verbose_status=DOCUMENT_TRACKING_DELETED_VERBOSE_STATUS,
        is_tracking=False
    )
    return document_refs


def delete_document(
        *,
        document: 'DocumentModel',
        preferences: 'UserPreferences' = None
) -> List[str]:
    return delete_documents(
       documents=[document],
       user_preferences=preferences
)


def get_document_pdf(
    *,
    document: 'DocumentModel',
    preferences: Union['UserPreferences', 'Preferences', None] = None
):
    client = get_prints_client(preferences=preferences)

    url = (
        client.get_document_url(
            number=document.int_doc_number
        )
    )
    file = create_temporary_file_from_url(url=url)

    document.pdf.save(
        f"document_{document.int_doc_number}.pdf",
        file
    )
    document.save()


def get_marking_pdf(
        *,
        document: 'DocumentModel',
        is_zebra_printer: bool = False,
        preferences: Union['UserPreferences', 'Preferences', None] = None
):
    client = get_prints_client(preferences=preferences)

    url = (
        client.get_marking_url(
            number=document.int_doc_number,
            is_zebra_printer=is_zebra_printer
        )
    )

    file = create_temporary_file_from_url(url=url)

    document.marking_pdf.save(
        f"marking_{document.int_doc_number}.pdf",
        file
    )
    document.save()


def track_actual_documents():
    PAGINATE_BY = 100
    documents = (
        DocumentModel
        .objects
        .filter(is_tracking=True)
    )
    paginator = Paginator(documents, PAGINATE_BY)
    for page_number in range(1, paginator.num_pages + 1):
        documents = paginator.page(page_number).object_list
        check_documents_status(documents=documents)


def get_document_price(
        *,
        preferences: Union['Preferences', 'UserPreferences'],
        city_sender: Union['City', 'Settlement'],
        city_recipient: Union['City', 'Settlement'],
        weight: float = DEFAULT_DOCUMENT_WEIGHT,
        service_type: 'ServiceType',
        cost: int,
        cargo_type: 'CargoType',
        seats_amount: int,
        redelivery_amount: int = None,
        redelivery_cargo_type: 'BackwardDeliveryCargoType' = None,
        **kwargs
) -> 'DocumentPrice':
    price_data = {
        "CitySender": city_sender.ref,
        "CityRecipient": city_recipient.ref,
        "Weight": weight,
        "ServiceType": service_type.ref,
        "Cost": cost,
        "CargoType": cargo_type.ref,
        "SeatsAmount": seats_amount,
        # "PackCalculate": {
        #     "PackCount": "1",
        #     "PackRef": "1499fa4a-d26e-11e1-95e4-0026b97ed48a"
        # },
        **kwargs
    }
    if redelivery_amount and redelivery_cargo_type:
        price_data["RedeliveryCalculate"]: {
            "CargoType": (
                redelivery_cargo_type.ref
                if redelivery_cargo_type
                else DEFAULT_BACKWARD_DELIVERY_CARGO_TYPE
            ),
            "Amount": redelivery_amount
        }

    client = get_document_client(preferences=preferences)
    return client.get_price(price_data)


def get_document_delivery_date(
        *,
        preferences: Union['Preferences', 'UserPreferences'],
        city_sender: 'City',
        city_recipient: 'City',
        service_type: 'ServiceType' = None,
        date_time: 'datetime' = None,

) -> 'DocumentDeliveryDate':
    delivery_date_data = {
        "ServiceType": (
            service_type.ref if service_type else DEFAULT_SERVICE_TYPE
        ),
        "CitySender": city_sender.ref,
        "CityRecipient": city_recipient.ref
    }

    if date_time:
        delivery_date_data['DateTime'] = (
            date_time.strftime('%d.%m.%Y')  # "10.11.2016",
        )

    client = get_document_client(preferences=preferences)

    return client.get_delivery_date(delivery_date_data)
