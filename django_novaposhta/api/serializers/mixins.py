from django.contrib.auth.models import AnonymousUser
from rest_framework import serializers

__all__ = (
    'DynamicFieldsSerializerMixin',
    'RequestSerializerMixin',
)


class DynamicFieldsSerializerMixin(serializers.Serializer):
    """
    Serializer mixin which allows pass to kwarg fields variable to return only specific fields
    """

    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)
        exclude_fields = kwargs.pop('exclude_fields', None)

        super().__init__(*args, **kwargs)

        existing = set(self.fields.keys())
        if exclude_fields:
            excluded = set(exclude_fields)
            for field_name in existing:
                if field_name in excluded:
                    self.fields.pop(field_name)
        elif fields:
            allowed = set(fields)
            for field_name in existing - allowed:
                if field_name in self.fields:
                    self.fields.pop(field_name)


class RequestSerializerMixin(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.view = self.context.get('view')
        self.request = self.context.get('request')
        self.user = getattr(self.request, 'user', AnonymousUser())
