from rest_framework import serializers

from .mixins import (
    DynamicFieldsSerializerMixin,
    RequestSerializerMixin,
)
from django_novaposhta.api.serializers import (
    CitySerializer,
    WarehouseSerializer,
    StreetSerializer,
    ServiceTypeSerializer,
    WarehouseTypeSerializer,
    PaymentFormSerializer,
    PayerTypeSerializer,
    CargoTypeSerializer,
)
from django_novaposhta.models import (
    UserPreferences,
    City,
    Warehouse,
    ServiceType,
    PayerType,
    CargoType,
    PaymentForm
)
from django_novaposhta.getters import get_default_payment_form_obj, get_user_preferences
from django_novaposhta.validators import (
    validate_preferences_service_type,
    populate_user_preferences, validate_user_preferences_exists,
)
from django_novaposhta.exceptions import NovaposhtaValidationError

__all__ = (
    'UserPreferencesActionSerializer',
    'UserPreferencesCreateSerializer',
    'UserPreferencesDisplaySerializer',
)


class UserPreferencesActionSerializer(
    RequestSerializerMixin,
    serializers.ModelSerializer
):
    """
    Note: at start create only warehouse sender address preferences
    """
    city = serializers.PrimaryKeyRelatedField(
        queryset=City.objects.all(), required=False
    )
    warehouse = serializers.PrimaryKeyRelatedField(
        queryset=Warehouse.objects.all(), required=False
    )
    service_type = serializers.PrimaryKeyRelatedField(
        queryset=ServiceType.objects.all(), required=False
    )
    payer_type = serializers.PrimaryKeyRelatedField(
        queryset=PayerType.objects.all(), required=False,
    )
    cargo_type = serializers.PrimaryKeyRelatedField(
        queryset=CargoType.objects.all(), required=False,
    )
    payment_form = serializers.PrimaryKeyRelatedField(
        queryset=PaymentForm.objects.all(), required=False,
    )

    class Meta:
        model = UserPreferences
        fields = (
            'api_key',
            'city',
            'warehouse',
            'street',
            'building',
            'flat',
            'service_type',
            'payer_type',
            'cargo_type',
            'payment_form',
        )

    def to_internal_value(self, data):
        data = super().to_internal_value(data)
        data['user'] = self.user
        return data

    def validate(self, attrs):
        attrs = super().validate(attrs)
        try:
            validate_preferences_service_type(attrs, self.instance)
            self.instance = populate_user_preferences(attrs, self.instance)
        except NovaposhtaValidationError as e:
            raise serializers.ValidationError(str(e))

        return attrs

    def update(self, instance, validated_data):
        validated_data.setdefault('payment_form', get_default_payment_form_obj())
        return super().update(instance, validated_data)


class UserPreferencesCreateSerializer(UserPreferencesActionSerializer):

    def validate(self, attrs):
        try:
            validate_user_preferences_exists(self.user)
        except NovaposhtaValidationError as e:
            raise serializers.ValidationError(str(e))
        return super().validate(attrs)


class WarehouseDisplaySerializer(WarehouseSerializer):
    type_of_warehouse = WarehouseTypeSerializer(
        fields=['id', 'description'],
    )


class UserPreferencesInvoiceInfoSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    middle_name = serializers.CharField()
    last_name = serializers.CharField()
    phone = serializers.CharField()
    email = serializers.EmailField()


class UserPreferencesDisplaySerializer(
    DynamicFieldsSerializerMixin,
    serializers.ModelSerializer
):
    city = CitySerializer(
        fields=['id', 'description', 'description_ru'],
    )
    street = StreetSerializer(
        fields=['id', 'description', 'description_ru'],
    )
    building = serializers.CharField()
    flat = serializers.CharField()
    warehouse = WarehouseDisplaySerializer(
        fields=[
            'id', 'description', 'description_ru',
            'area', 'type_of_warehouse'
        ],
    )
    service_type = ServiceTypeSerializer(
        fields=['id', 'description'],
    )
    payer_type = PayerTypeSerializer(
        fields=['id', 'description'],
    )
    cargo_type = CargoTypeSerializer(
        fields=['id', 'description'],
    )
    payment_form = PaymentFormSerializer(
        fields=['id', 'description'],
    )
    invoice_info = serializers.SerializerMethodField()

    class Meta:
        model = UserPreferences
        fields = (
            'id',
            'api_key',
            'is_active',
            'city',
            'street',
            'building',
            'flat',
            'warehouse',
            'invoice_info',
            'service_type',
            'payer_type',
            'payment_form',
            'cargo_type',
        )

    def get_invoice_info(self, obj):
        return UserPreferencesInvoiceInfoSerializer(obj).data
