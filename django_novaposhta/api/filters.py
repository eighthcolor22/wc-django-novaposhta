import operator
from functools import reduce

import django_filters
from django.db import models

from django_filters import filterset
from django_filters.constants import EMPTY_VALUES
from rest_framework.filters import SearchFilter
from rest_framework.compat import distinct

from ..models import Street, SettlementStreet
from ..services.addresses import search_settlements_streets


__all__ = (
    'OrderingSearchFilter',
    'StreetsFilterSet',
)


class OrderingSearchFilter(SearchFilter):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ordering_field = 'description'

    def get_ordering(self, request, queryset, view):
        return 'id'

    def filter_queryset(self, request, queryset, view):
        if hasattr(view, 'ordering_search_field'):
            self.ordering_field = (
                getattr(
                    view,
                    'ordering_search_field',
                    self.ordering_field
                )
            )

        search_fields = getattr(view, 'search_fields', None)
        search_terms = self.get_search_terms(request)

        if not search_fields or not search_terms:
            return queryset
        orm_lookups = [
            self.construct_search(str(search_field))
            for search_field in search_fields
        ]

        base = queryset
        conditions = []

        for search_term in search_terms:
            queries = [
                models.Q(**{orm_lookup: search_term})
                for orm_lookup in orm_lookups
            ]
            conditions.append(reduce(operator.or_, queries))

        queryset = queryset.filter(reduce(operator.and_, conditions))

        if self.must_call_distinct(queryset, search_fields):
            queryset = distinct(queryset, base)

        search_terms = ' '.join(search_terms)

        iexact_lookup = models.Q(**{f'{self.ordering_field}__iexact': search_terms})
        istartswith__lookup = models.Q(**{f'{self.ordering_field}__istartswith': search_terms})

        if hasattr(queryset.model, f'{self.ordering_field}_ru'):
            iexact_lookup = (
                iexact_lookup | models.Q(**{f'{self.ordering_field}_ru__iexact': search_terms})
            )
            istartswith__lookup = (
                istartswith__lookup | models.Q(**{f'{self.ordering_field}_ru__istartswith': search_terms})
            )

        queryset = (
            queryset.annotate(
                # set priority to order by
                priority=models.Case(
                    models.When(
                        iexact_lookup,
                        then=models.Value(1)
                    ),
                    models.When(
                        istartswith__lookup,
                        then=models.Value(2)
                    ),
                    default=models.Value(3),
                    output_field=models.SmallIntegerField()
                )
            )
            .order_by('priority')
        )

        return queryset


class StreetsFilterSet(filterset.FilterSet):
    settlement_ref = django_filters.CharFilter(
        method='settlement_ref_filter_method',
    )

    class Meta:
        model = Street
        fields = (
            'city',
            'streets_type_ref',
            'city__ref',
            'settlement_ref',
        )

    def settlement_ref_filter_method(self, qs, name, value):
        if value in EMPTY_VALUES:
            return qs
        # using city-warehouse relation may not looks good ,
        # but it is so far the only one inside the app
        potential_streets_qs = qs.filter(city__warehouses__settlement_ref=value)
        if potential_streets_qs.exists():
            return potential_streets_qs.distinct()

        street_name_search = self.data.getlist('search')[0]
        if street_name_search:
            settlement_streets: list = search_settlements_streets(
                settlement_ref=value,
                street_name=street_name_search
            )
            return (
                SettlementStreet
                .objects
                .filter(
                    id__in=[item.id for item in settlement_streets]
                )
            )
        return qs
    