from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    UpdateAPIView,
)
from rest_framework.response import Response
from rest_framework import status
from standards.drf.views import StandardAPIViewMixin

from ..serializers.preferences import (
    UserPreferencesActionSerializer,
    UserPreferencesDisplaySerializer,
    UserPreferencesCreateSerializer,
)
from ...models import UserPreferences

__all__ = (
    'UserPreferencesBaseRetrieveAPIView',
    'UserPreferencesBaseCreateAPIView',
    'UserPreferencesBaseUpdateAPIView',
)


class UserPreferencesGetterMixin:
    queryset = UserPreferences.objects.all()

    def get_object(self):
        user = self.request.user
        return (
            self.queryset
            .filter(user_id=user.id)
            .first()
        )


class UserPreferencesBaseRetrieveAPIView(
    UserPreferencesGetterMixin,
    StandardAPIViewMixin,
    RetrieveAPIView
):
    serializer_class = UserPreferencesDisplaySerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance:
            return Response()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class UserPreferencesActionMixin:
    serializer_class = UserPreferencesActionSerializer
    result_serializer_class = UserPreferencesDisplaySerializer


class UserPreferencesBaseCreateAPIView(
    UserPreferencesActionMixin,
    StandardAPIViewMixin,
    CreateAPIView
):
    serializer_class = UserPreferencesCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        data = (
            self.result_serializer_class(
                instance=serializer.instance,
                context=self.get_serializer_context()
            ).data
        )

        headers = self.get_success_headers(data)
        return Response(data, status=status.HTTP_201_CREATED, headers=headers)


class UserPreferencesBaseUpdateAPIView(
    UserPreferencesGetterMixin,
    UserPreferencesActionMixin,
    StandardAPIViewMixin,
    UpdateAPIView
):

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        data = (
            self.result_serializer_class(
                instance=serializer.instance,
                context=self.get_serializer_context()
            ).data
        )
        return Response(data)





