from django.utils.decorators import method_decorator

from rest_framework.generics import (
    ListAPIView,
    DestroyAPIView,
)
from rest_framework.serializers import Serializer
from standards.drf.views import StandardAPIViewMixin

from .shared import (
    ActionGenericAPIView,
    NovaposhtaPreferencesGetterViewMixin,
)
from ..serializers.scan_sheets import (
    NovaposhtaScanSheetRetrieveSerializer,
    ScanSheetDocumentsSerializer,
    InsertDocumentsSerializer,
)
from ..decorators import handle_novaposhta_exception
from ...models import ScanSheet
from ...services.scan_sheets import (
    insert_documents,
    expand_scan_sheet,
    remove_documents,
    delete_scan_sheet,
    get_scan_sheet,
    get_scan_sheet_pdf,
    get_scan_sheet_documents_markings_zebra_pdf,
)

__all__ = (
    'ScanSheetBaseListAPIView',
    'InsertDocumentToScanSheetBaseAPIView',
    'ScanSheetBaseDeleteAPIView',
    'RemoveDocumentsFromScanSheetBaseAPIView',
    'ScanSheetSyncBaseAPIView',
    'ScanSheetPrintPDFBaseAPIView',
    'ScanSheetPrintMarkingBaseAPIView',
)


class ScanSheetBaseListAPIView(
    StandardAPIViewMixin,
    ListAPIView
):
    serializer_class = NovaposhtaScanSheetRetrieveSerializer
    queryset = ScanSheet.objects.all()


class ScanSheetResponseViewMixin:

    def get_response(self, result, serializer):
        return NovaposhtaScanSheetRetrieveSerializer(result).data


@method_decorator(handle_novaposhta_exception, name='post')
class InsertDocumentToScanSheetBaseAPIView(
    ScanSheetResponseViewMixin,
    NovaposhtaPreferencesGetterViewMixin,
    StandardAPIViewMixin,
    ActionGenericAPIView,
):
    serializer_class = InsertDocumentsSerializer

    def perform_action(self, serializer):
        documents = serializer.validated_data['documents']
        scan_sheet = serializer.validated_data.get('scan_sheet')
        if scan_sheet:
            scan_sheet = expand_scan_sheet(
                documents=documents,
                scan_sheet=scan_sheet,
                preferences=self.get_preferences()
            )
        else:
            scan_sheet = insert_documents(
                documents=documents,
                preferences=self.get_preferences()
            )
        return scan_sheet


class ScanSheetBaseDeleteAPIView(
    NovaposhtaPreferencesGetterViewMixin,
    StandardAPIViewMixin,
    DestroyAPIView
):
    queryset = ScanSheet.objects.all()

    def after_destroy(self, instance):
        pass

    @handle_novaposhta_exception
    def perform_destroy(self, instance):
        delete_scan_sheet(
            scan_sheet=instance,
            preferences=self.get_preferences()
        )
        self.after_destroy(instance)


@method_decorator(handle_novaposhta_exception, name='post')
class RemoveDocumentsFromScanSheetBaseAPIView(
    NovaposhtaPreferencesGetterViewMixin,
    StandardAPIViewMixin,
    ActionGenericAPIView,
):
    serializer_class = ScanSheetDocumentsSerializer

    def perform_action(self, serializer):
        remove_documents(
            documents=serializer.validated_data['documents'],
            preferences=self.get_preferences()
        )


@method_decorator(handle_novaposhta_exception, name='post')
class ScanSheetSyncBaseAPIView(
    ScanSheetResponseViewMixin,
    NovaposhtaPreferencesGetterViewMixin,
    StandardAPIViewMixin,
    ActionGenericAPIView,
):
    serializer_class = Serializer
    queryset = ScanSheet.objects.all()

    def perform_action(self, serializer):
        scan_sheet = get_scan_sheet(
            scan_sheet=self.get_object(),
            preferences=self.get_preferences()
        )
        return scan_sheet


@method_decorator(handle_novaposhta_exception, name='post')
class ScanSheetPrintPDFBaseAPIView(
    ScanSheetResponseViewMixin,
    NovaposhtaPreferencesGetterViewMixin,
    StandardAPIViewMixin,
    ActionGenericAPIView,
):
    serializer_class = Serializer
    queryset = ScanSheet.objects.all()

    def perform_action(self, serializer):
        scan_sheet = self.get_object()
        get_scan_sheet_pdf(
            scan_sheet=scan_sheet,
            preferences=self.get_preferences()
        )
        scan_sheet.refresh_from_db()
        return scan_sheet


@method_decorator(handle_novaposhta_exception, name='post')
class ScanSheetPrintMarkingBaseAPIView(
    ScanSheetResponseViewMixin,
    NovaposhtaPreferencesGetterViewMixin,
    StandardAPIViewMixin,
    ActionGenericAPIView,
):
    serializer_class = Serializer
    queryset = ScanSheet.objects.all()

    def perform_action(self, serializer):
        scan_sheet = self.get_object()
        get_scan_sheet_documents_markings_zebra_pdf(
            scan_sheet=scan_sheet,
            preferences=self.get_preferences()
        )
        scan_sheet.refresh_from_db()
        return scan_sheet
