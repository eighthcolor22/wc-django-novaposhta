from django.utils.decorators import method_decorator

from rest_framework import serializers
from rest_framework.generics import (
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveAPIView,
)
from rest_framework.response import Response
from rest_framework import status
from standards.drf.views import StandardAPIViewMixin

from .shared import (
    ActionGenericAPIView,
    NovaposhtaPreferencesGetterViewMixin,
)
from ..serializers.documents import (
    DocumentRetrieveSerializer,
    NovaposhtaDocumentPrintMarkingSerializer
)
from ..decorators import handle_novaposhta_exception
from ...models import Document
from ...services.documents import (
    check_documents_status,
    get_document_pdf,
    get_marking_pdf,
    delete_document,
)


__all__ = (
    'DocumentBaseCreateAPIView',
    'DocumentBaseUpdateAPIView',
    'DocumentBaseCheckStatusAPIView',
    'DocumentBaseDeleteAPIView',
    'DocumentPrintPDFBaseAPIView',
    'DocumentPrintMarkingPDFBaseAPIView',
    'DocumentBaseRetrieveAPIView',
)


class DocumentResponseSerializerMixin(NovaposhtaPreferencesGetterViewMixin):

    def get_document_response_data(self, instance):
        serializer = DocumentRetrieveSerializer(instance)
        return serializer.data


class DocumentBaseCreateAPIView(
    DocumentResponseSerializerMixin,
    StandardAPIViewMixin,
    CreateAPIView,
):
    """
    Base view for creating novaposhta documents. It is suggested that
     'django_novaposhta.services.document.create_document' method calls in
     serializer.save() and return DocumentModel instance
    """
    serializer_class = serializers.Serializer

    def perform_create(self, serializer):
        return serializer.save()

    @handle_novaposhta_exception
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        document = self.perform_create(serializer)

        return Response(
            data=self.get_document_response_data(document),
            status=status.HTTP_201_CREATED,
        )


class DocumentBaseUpdateAPIView(
    DocumentResponseSerializerMixin,
    StandardAPIViewMixin,
    UpdateAPIView,
):
    """
    Base view for creating novaposhta documents. It is suggested that
     'django_novaposhta.services.document.update_document' method calls in
     serializer.save() and return DocumentModel instance
    """
    serializer_class = serializers.Serializer
    queryset = Document.objects.all()

    def perform_update(self, serializer):
        return serializer.save()

    @handle_novaposhta_exception
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=False
        )
        serializer.is_valid(raise_exception=True)
        document = self.perform_update(serializer)
        return Response(
            data=self.get_document_response_data(document)
        )


class DocumentBaseCheckStatusAPIView(DocumentBaseUpdateAPIView):

    @handle_novaposhta_exception
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        check_documents_status(
            documents=[instance],
            preferences=self.get_preferences()
        )
        instance.refresh_from_db()
        return Response(
            self.get_document_response_data(instance)
        )


class DocumentBaseDeleteAPIView(
    NovaposhtaPreferencesGetterViewMixin,
    StandardAPIViewMixin,
    DestroyAPIView
):
    queryset = Document.objects.all()

    def after_destroy(self, instance):
        pass

    @handle_novaposhta_exception
    def perform_destroy(self, instance):
        delete_document(
            document=instance,
            preferences=self.get_preferences()
        )
        self.after_destroy(instance)


@method_decorator(handle_novaposhta_exception, name='post')
class DocumentPrintPDFBaseAPIView(
    DocumentResponseSerializerMixin,
    StandardAPIViewMixin,
    ActionGenericAPIView,
):
    serializer_class = serializers.Serializer
    queryset = Document.objects.all()

    def perform_action(self, serializer):
        document = self.get_object()
        get_document_pdf(
            document=document,
            preferences=self.get_preferences()
        )
        document.refresh_from_db()
        return document

    def get_response(self, result, serializer):
        return self.get_document_response_data(result)


class DocumentPrintMarkingPDFBaseAPIView(
    DocumentPrintPDFBaseAPIView,
):
    serializer_class = NovaposhtaDocumentPrintMarkingSerializer
    
    def perform_action(self, serializer):
        validated_data = serializer.validated_data
        document = self.get_object()
        get_marking_pdf(
            document=document,
            is_zebra_printer=validated_data.get('is_zebra_printer', False),
            preferences=self.get_preferences()
        )
        document.refresh_from_db()
        return document


class DocumentBaseRetrieveAPIView(
    StandardAPIViewMixin,
    RetrieveAPIView
):
    serializer_class = DocumentRetrieveSerializer
    queryset = Document.objects.all()
