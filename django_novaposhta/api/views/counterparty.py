from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from standards.drf.views import StandardAPIViewMixin

from ..serializers.counterparty import (
    CounterpartyCreateSerializer
)
from ...models import (
    Counterparty,
)

__all__ = (
    "CounterpartyCreateAPIView"
)


class CounterpartyCreateAPIView(StandardAPIViewMixin, CreateAPIView):
    permission_classes = (IsAuthenticated, )
    queryset = Counterparty
    serializer_class = CounterpartyCreateSerializer
