__all__ = (
    'NovaposhtaException',
    'NovaposhtaResponseError',
    'NovaposhtaValidationError',
)


class NovaposhtaException(Exception):
    pass


class NovaposhtaResponseError(NovaposhtaException):
    pass


class NovaposhtaValidationError(NovaposhtaException):
    pass
