from django.urls import path, include

from . import views

app_name = "novaposhta"

urlpatterns = [
    path('novaposhta/', include([
        path('autocomplete/', include(([
              path('cities/', views.CityAutocomplete.as_view(), name='cities'),
              path('warehouses/', views.WarehousesAutocomplete.as_view(), name='warehouses'),
              path('streets/', views.StreetsAutocomplete.as_view(), name='streets'),
          ], 'autocomplete'))),
    ]))
]
