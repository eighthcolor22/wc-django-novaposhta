from .services import (
    import_areas,
    import_backward_delivery_cargo_types,
    import_cargo_types,
    import_cities,
    import_settlements,
    import_warehouses,
    import_warehouse_types,
    import_streets,
    import_payer_types,
    import_payment_forms,
    import_service_types,
    import_counterparty_types,
    populate_warehouses_with_settlement_ref
)

__all__ = (
    'update_novaposhta',
)


def update_novaposhta():
    import_payer_types()
    import_payment_forms()
    import_backward_delivery_cargo_types()
    import_cargo_types()
    import_service_types()
    import_counterparty_types()
    import_areas()
    import_cities()
    # to populate areas center
    import_areas()
    import_settlements()
    import_warehouse_types()
    import_warehouses()
    import_streets()

    populate_warehouses_with_settlement_ref()
