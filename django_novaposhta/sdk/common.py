from typing import Iterator, Type, TYPE_CHECKING, Union

from .base import BaseServiceAdapter
from ..client import SyncRequestClient
from ..entities import CatalogItem
from ..models import Preferences, UserPreferences

if TYPE_CHECKING:
    from ..client import BaseRequestClient

__all__ = (
    'CommonService',
    'get_common_client'
)


class CommonService(BaseServiceAdapter):
    MODEL_NAME = 'Common'

    def _get_common_items(
            self, called_method: str
    ) -> Iterator['CatalogItem']:
        response: 'Iterator' = self.make_request(
            called_method=called_method,
            entity_class=CatalogItem
        )

        return response

    def get_payer_types(self) -> Iterator['CatalogItem']:
        return self._get_common_items("getTypesOfPayers")

    def get_payment_forms(self) -> Iterator['CatalogItem']:
        return self._get_common_items("getPaymentForms")

    def get_cargo_types(self) -> Iterator['CatalogItem']:
        return self._get_common_items("getCargoTypes")

    def get_backward_delivery_cargo_types(
            self
    ) -> Iterator['CatalogItem']:
        return self._get_common_items("getBackwardDeliveryCargoTypes")

    def get_service_types(self) -> Iterator['CatalogItem']:
        return self._get_common_items("getServiceTypes")

    def get_counterparties_types(self) -> Iterator['CatalogItem']:
        return self._get_common_items("getTypesOfCounterparties")


def get_common_client(
        request_client_class: Type['BaseRequestClient'] = SyncRequestClient,
        preferences: Union['UserPreferences', 'Preferences', None] = None
) -> 'CommonService':
    preferences = preferences or Preferences.get_solo()
    client = CommonService(
        api_key=preferences.api_key,
        request_client_class=request_client_class
    )
    return client
