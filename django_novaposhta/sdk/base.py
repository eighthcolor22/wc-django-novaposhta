from typing import (
    Dict,
    Iterator,
    Optional,
    List,
    Type,
    TypeVar,
    Tuple,
    TYPE_CHECKING,
    Union
)

from ..client import (
    Response,
    SyncRequestClient
)
from ..consts import API_V2_URL
from ..entities import BaseImportEntity
from ..exceptions import NovaposhtaResponseError
from ..utils import clean_dict


if TYPE_CHECKING:
    from ..client import BaseRequestClient

__all__ = (
    'BaseServiceAdapter',
)


BaseImportEntityVar = TypeVar('BaseImportEntityVar', bound=BaseImportEntity)


class BaseServiceAdapter:
    MODEL_NAME: str

    def __init__(
            self,
            api_key: str,
            api_url: str = API_V2_URL,
            request_client_class: Type['BaseRequestClient'] = SyncRequestClient
    ):
        self.api_key = api_key
        self.request_client = request_client_class(api_url=api_url)

    def check_errors(self, response_json: Dict):
        errors = response_json.get("errors")

        if errors:
            raise NovaposhtaResponseError(errors)

    def _make_request(
            self,
            called_method: str,
            request_method: str = 'post',
            method_properties: Optional[Dict] = None,

    ) -> dict:
        # print('-*-*-*-*-*-*-*-*-*-*-*-**-*-*-**-**-*-*-*-*-*-*-*-*-**-*-*-*-*-*')
        # print('request_data= ', {
        #         "apiKey": self.api_key,
        #         "modelName": self.MODEL_NAME,
        #         "calledMethod": called_method,
        #         "methodProperties": clean_dict(method_properties),
        #     })
        response: 'Response' = self.request_client.make_request(
            method=request_method,
            json={
                "apiKey": self.api_key,
                "modelName": self.MODEL_NAME,
                "calledMethod": called_method,
                "methodProperties": clean_dict(method_properties),
            }
        )

        response_json: 'Dict' = response.response_data

        print(f'{response_json=}')

        self.check_errors(response_json)
        return response_json

    @staticmethod
    def _generate_entities_from_response(
        response_data: List,
        entity_class: Type['BaseImportEntity'],
        key: str = None,
        **kwargs
    ):
        return (
            entity_class(
                **(item[key] if key else item),
                **kwargs
            )
            for item in response_data
        )

    def make_request(
            self,
            called_method: str,
            entity_class: Type['BaseImportEntity'] = None,
            method_properties: Optional[Dict] = None,
            key: str = None,
            is_single: bool = False,
            request_method: str = 'post',
            **kwargs
    ) -> Union['BaseImportEntityVar', Iterator['BaseImportEntityVar'], Dict, List]:

        response_json: 'Dict' = self._make_request(
            called_method=called_method,
            method_properties=method_properties,
            request_method=request_method,
        )

        response_data: List = response_json['data']

        if entity_class:
            data = self._generate_entities_from_response(
                response_data=response_data,
                entity_class=entity_class,
                key=key,
                **kwargs
            )

            if is_single:
                return next(data, {})

            return data

        return response_data



    # def get_objects(
    #         self,
    #         called_method: str,
    #         method_properties: Optional[Dict] = None,
    #         page: int = 1,
    #         delay: float = None
    # ) -> Generator[Dict, None, None]:
    #     if not method_properties:
    #         method_properties = {"Page": page}
    #     else:
    #         method_properties["Page"] = page
    #
    #     while True:
    #         data: List = self.make_request(
    #             called_method=called_method,
    #             method_properties=method_properties
    #         )
    #
    #         if not data:
    #             break
    #
    #         method_properties["Page"] += 1
    #
    #         for item in data:
    #             yield item
    #
    #         if delay:
    #             time.sleep(delay)
