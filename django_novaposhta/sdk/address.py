from typing import (
    Dict, Iterator, Optional, List,
    Type, TYPE_CHECKING, Union, Tuple
)

from .base import BaseServiceAdapter
from ..client import SyncRequestClient
from ..entities import (
    Area,
    BaseImportEntity,
    City,
    CatalogItem,
    Street,
    Settlement,
    Warehouse,
    SettlementStreet,
    LiveSearchSettlement,
)
from ..models import (
    UserPreferences,
    Preferences,
)

if TYPE_CHECKING:
    from ..client import BaseRequestClient

__all__ = (
    'AddressService',
    'get_address_client'
)


class AddressService(BaseServiceAdapter):
    MODEL_NAME = 'Address'

    def _get_address_items(
            self,
            called_method: str,
            entity_class: Type['BaseImportEntity'],
            method_properties: Optional[Dict] = None,
            page: int = 1,
            **kwargs
    ) -> Iterator:
        if not method_properties:
            method_properties = {"Page": page}
        else:
            method_properties["Page"] = page

        while True:
            data: Iterator = self.make_request(
                called_method=called_method,
                entity_class=entity_class,
                method_properties=method_properties,
                **kwargs
            )

            first_item = next(data, None)

            if not first_item:
                break

            method_properties["Page"] += 1

            yield first_item

            for item in data:
                yield item

    def get_areas(self, page: int = 1) -> Iterator["Area"]:
        return self._get_address_items(
            called_method="getAreas",
            entity_class=Area,
            page=page
        )

    def get_settlements(
            self, page: int = 1
    ) -> Iterator["City"]:
        return self._get_address_items(
            called_method="getSettlements",
            entity_class=Settlement,
            page=page
        )

    def get_cities(self, page: int = 1) -> Iterator["City"]:
        return self._get_address_items(
            called_method="getCities",
            entity_class=City,
            page=page
        )

    def get_streets(
            self, city_ref: str, page: int = 1
    ) -> Iterator['Street']:
        return self._get_address_items(
            called_method="getStreet",
            entity_class=Street,
            method_properties={"CityRef": city_ref},
            page=page,
            CityRef=city_ref
        )

    def get_warehouses(self, page: int = 1, properties: Dict = None) -> Iterator['Warehouse']:
        method_properties = {"Limit": 500}

        if properties:
            method_properties.update(properties)

        return self._get_address_items(
            called_method="getWarehouses",
            entity_class=Warehouse,
            method_properties=method_properties,
            page=page,
        )

    def get_warehouse_types(self) -> Iterator['CatalogItem']:
        response: 'Iterator' = self.make_request(
            called_method="getWarehouseTypes",
            entity_class=CatalogItem
        )

        for item in response:
            yield item

    def save(
            self,
            counterparty_ref: str,
            street_ref: str,
            building_number: str,
            flat: str,
            note: str = ''
    ) -> 'CatalogItem':
        """
        Создать адрес контрагента (отправитель/получатель)

        :param counterparty_ref: Идентификатор контрагента
        :param street_ref: Идентификатор улицы
        :param building_number: Номер дома
        :param flat: Квартира
        :param note: Комментарий
        :return: List['DirectoryItem']
        """
        response: 'CatalogItem' = self.make_request(
            called_method="save",
            entity_class=CatalogItem,
            method_properties={
                'CounterpartyRef': counterparty_ref,
                'StreetRef': street_ref,
                'BuildingNumber': building_number,
                'Flat': flat,
                'Note': note
            },
            is_single=True
        )

        return response

    def update(
            self,
            ref: str,
            counterparty_ref: str,
            street_ref: str,
            building_number: str,
            flat: str,
            note: str
    ) -> 'CatalogItem':
        """
        Обновить адрес контрагента (отправитель/получатель)

        :param ref: Идентификатор адреса
        :param counterparty_ref: Идентификатор контрагента
        :param street_ref: Идентификатор улицы
        :param building_number: Номер дома
        :param flat: Квартира
        :param note: Комментарий
        :return: List['DirectoryItem']
        """
        response: 'CatalogItem' = self.make_request(
            called_method="update",
            entity_class=CatalogItem,
            method_properties={
                'Ref': ref,
                'CounterpartyRef': counterparty_ref,
                'StreetRef': street_ref,
                'BuildingNumber': building_number,
                'Flat': flat,
                'Note': note
            },
            is_single=True
        )

        return response

    def _make_address_search_request(
            self,
            called_method: str,
            entity_class: Type['BaseImportEntity'],
            method_properties: Optional[Dict] = None,
            key: str = None,
            request_method: str = 'post',
            **kwargs
    ) -> Tuple[Iterator['BaseImportEntityVar'], Optional[int]]:
        response_json: 'Dict' = self._make_request(
            called_method=called_method,
            method_properties=method_properties,
            request_method=request_method,
        )
        response_data: List = response_json['data']
        addresses_data = response_data[0]['Addresses']
        data = self._generate_entities_from_response(
            response_data=addresses_data,
            entity_class=entity_class,
            key=key,
            **kwargs
        )
        total_count = response_data[0].get('TotalCount')
        return data, total_count

    def _search_address_items(
            self,
            called_method: str,
            entity_class: Type['BaseImportEntity'],
            method_properties: Optional[Dict] = None,
            **kwargs
    ) -> Tuple[Iterator, Optional[int]]:

        return self._make_address_search_request(
            called_method=called_method,
            entity_class=entity_class,
            method_properties=method_properties,
            **kwargs
        )

    def search_settlements_streets(
        self,
        settlement_ref: str,
        street_name: str,
    ) -> Tuple[Iterator, Optional[int]]:
        response = self._search_address_items(
            called_method="searchSettlementStreets",
            entity_class=SettlementStreet,
            method_properties={
                "StreetName": street_name,
                "SettlementRef": settlement_ref
            },
        )
        return response

    def search_settlements(
        self,
        city_name: str,
        limit: int = 50,
        page: int = 1
    ) -> Tuple[Iterator, Optional[int]]:
        response = self._search_address_items(
            called_method="searchSettlements",
            entity_class=LiveSearchSettlement,
            method_properties={
                "CityName": city_name,
                "Limit": limit,
                "page": page
            },
        )
        return response


def get_address_client(
        request_client_class: Type['BaseRequestClient'] = SyncRequestClient,
        preferences: Union['UserPreferences', 'Preferences', None] = None
) -> 'AddressService':
    preferences = preferences or Preferences.get_solo()
    client = AddressService(
        api_key=preferences.api_key,
        request_client_class=request_client_class
    )
    return client
