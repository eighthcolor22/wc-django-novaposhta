from typing import Dict, Iterator, List, Type, TYPE_CHECKING, Union

from .base import BaseServiceAdapter
from ..client import SyncRequestClient
from ..entities import (
    CatalogItem,
    Document,
    DocumentPrice,
    DocumentDeliveryDate
)
from ..models import Preferences, UserPreferences

if TYPE_CHECKING:
    from ..client import BaseRequestClient

__all__ = (
    'DocumentService',
    'get_document_client'
)


class DocumentService(BaseServiceAdapter):
    MODEL_NAME = 'InternetDocument'

    def save(self, data: Dict) -> 'Document':
        response: 'Document' = self.make_request(
            called_method="save",
            entity_class=Document,
            method_properties=data,
            is_single=True
        )

        return response

    def update(self, data: Dict) -> 'Document':
        response: 'Document' = self.make_request(
            called_method="update",
            entity_class=Document,
            method_properties=data,
            is_single=True
        )

        return response

    def delete(self, refs: List[str]) -> Iterator['CatalogItem']:
        response: Iterator['CatalogItem'] = self.make_request(
            called_method="delete",
            entity_class=CatalogItem,
            method_properties={"DocumentRefs": refs}
        )
        return response

    def get_price(self, data: Dict) -> 'DocumentPrice':
        response: 'DocumentPrice' = self.make_request(
            called_method="getDocumentPrice",
            entity_class=DocumentPrice,
            method_properties=data,
            is_single=True
        )

        return response

    def get_delivery_date(
            self, data: Dict
    ) -> 'DocumentDeliveryDate':
        response: 'DocumentDeliveryDate' = self.make_request(
            called_method="getDocumentDeliveryDate",
            entity_class=DocumentDeliveryDate,
            method_properties=data,
            key='DeliveryDate',
            is_single=True
        )

        return response


def get_document_client(
        request_client_class: Type['BaseRequestClient'] = SyncRequestClient,
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> 'DocumentService':
    preferences = preferences or Preferences.get_solo()
    client = DocumentService(
        api_key=preferences.api_key,
        request_client_class=request_client_class
    )

    return client
