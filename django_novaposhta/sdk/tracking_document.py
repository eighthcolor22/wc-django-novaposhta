from typing import Iterator, List, Type, TYPE_CHECKING, Optional

from .base import BaseServiceAdapter
from ..client import SyncRequestClient
from ..consts import TRACK_DOCUMENT_STATUS_LIMIT
from ..entities import DocumentStatus
from ..models import Preferences, UserPreferences

if TYPE_CHECKING:
    from ..client import BaseRequestClient

__all__ = (
    'TrackingDocumentService',
    'get_tracking_document_client'
)


class TrackingDocumentService(BaseServiceAdapter):
    MODEL_NAME = 'TrackingDocument'

    def track_document_status(
            self, numbers: List[str]
    ) -> Iterator['DocumentStatus']:
        for index in range(0, len(numbers), TRACK_DOCUMENT_STATUS_LIMIT):
            response: 'List' = self.make_request(
                called_method="getStatusDocuments",
                entity_class=DocumentStatus,
                method_properties={
                    "Documents": [
                        {
                            "DocumentNumber": number,
                            "phone": ""
                        }
                        for number in
                        numbers[index:index + TRACK_DOCUMENT_STATUS_LIMIT]
                    ]
                },
            )

            for item in response:
                yield item


def get_tracking_document_client(
        request_client_class: Type['BaseRequestClient'] = SyncRequestClient,
        preferences: Optional['UserPreferences'] = None
) -> 'TrackingDocumentService':
    preferences = preferences or Preferences.get_solo()
    print('-------------------------------------------------------')
    print(f'{preferences=}')
    client = TrackingDocumentService(
        api_key=preferences.api_key,
        request_client_class=request_client_class
    )

    return client
