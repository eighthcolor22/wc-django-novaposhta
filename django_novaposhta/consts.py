from django.utils.translation import pgettext_lazy

__all__ = (
    'API_V2_URL',
    'JSON_RESPONSE_CONTENT_TYPE',
    'PRINT_DOCUMENT_URL',
    'PRINT_DOCUMENTS_URL',
    'PRINT_MARKING_URL',
    'PRINT_MARKINGS_URL',
    'PRINT_SCAN_SHEET_URL',
    'DOCUMENT_TRACKING_DEFAULT_STATUS',
    'DOCUMENT_TRACKING_DELETED_STATUS',
    'DOCUMENT_TRACKING_DEFAULT_VERBOSE_STATUS',
    'DOCUMENT_TRACKING_DELETED_VERBOSE_STATUS',
    'DOCUMENT_TRACKING_DISABLE_STATUSES',
    'DOCUMENT_TRACKING_STATUSES',
    'TRACK_DOCUMENT_STATUS_LIMIT',
    'DEFAULT_CARGO_TYPE',
    'RECIPIENT_REF',
    'DEFAULT_DESCRIPTION',
    'DEFAULT_PAYER_TYPE',
    'DEFAULT_PAYMENT_METHOD',
    'DEFAULT_SERVICE_TYPE',
    'DEFAULT_COUNTERPARTY_TYPE',
    'DEFAULT_BACKWARD_DELIVERY_CARGO_TYPE',
    'DEFAULT_DOCUMENT_WEIGHT',
    'COUNTERPARTY_PRIVATE_PERSON_TYPE',
    'WAREHOUSE', 'DOORS', 'POSTOMAT',
    'WAREHOUSE_DEPARTURE_SERVICE_TYPES_MAP',
    'DOORS_DEPARTURE_SERVICE_TYPES_MAP',
    'WAREHOUSE_PACKSTATION_TYPE_REF',
    'NOVAPOSHTA_WEIGHT_VOLUME_RATIO',
    'NOVAPOSHTA_MINIMAL_ESTIMATED_COST',
    'NOVAPOSHTA_REQUEST_THROTTLING_DEFAULT_TIME',
    'NOVAPOSHTA_TIME_ZONE',
)


API_V2_URL = "https://api.novaposhta.ua/v2.0/json/"
JSON_RESPONSE_CONTENT_TYPE = 'application/json'
PRINT_DOCUMENT_URL = (
    "https://my.novaposhta.ua/orders/"
    "printDocument/orders[]/{number}/type/{type}/apiKey/{api_key}"
)
PRINT_DOCUMENTS_URL = (
    "https://my.novaposhta.ua/orders/"
    "printDocument/orders/{numbers}/type/{type}/apiKey/{api_key}"
)
PRINT_MARKING_URL = (
    "https://my.novaposhta.ua/orders/"
    "printMarking{size}/orders[]/{number}/type/{type}/apiKey/{api_key}"
)
PRINT_MARKINGS_URL = (
    "https://my.novaposhta.ua/orders/"
    "printMarking{size}/orders/{numbers}/type/{type}/apiKey/{api_key}"
)
PRINT_SCAN_SHEET_URL = (
    "https://my.novaposhta.ua/scanSheet/"
    "printScanSheet/refs[]/{ref}/type/{type}/apiKey/{api_key}"
)

DOCUMENT_TRACKING_DEFAULT_STATUS = "1"
DOCUMENT_TRACKING_DELETED_STATUS = "2"
DOCUMENT_TRACKING_DEFAULT_VERBOSE_STATUS = "Нова пошта очікує надходження від відправника"
DOCUMENT_TRACKING_DELETED_VERBOSE_STATUS = "Видалено"
DOCUMENT_TRACKING_DISABLE_STATUSES = ("2", "3", "9", "10", "11", "14")

DOCUMENT_TRACKING_STATUSES = (
    (DOCUMENT_TRACKING_DEFAULT_STATUS, pgettext_lazy('novaposhta', "Pending.")),
    ("2", pgettext_lazy('novaposhta', "Deleted.")),
    ("3", pgettext_lazy('novaposhta', "Not found.")),
    (
        "4",
        pgettext_lazy(
            'novaposhta',
            "Departure in the city of XXXX "
            "(Status for interregional shipments)."
        )
    ),
    (
        "41",
        pgettext_lazy(
            'novaposhta',
            """
            Departure in the city of XXXX 
            (Status for services is locale standard 
            and locale express delivery within the city).
            """
        ),
    ),
    ("5", pgettext_lazy('novaposhta', "Departing to YYYY.")),
    (
        "6",
        pgettext_lazy(
            'novaposhta',
            """
            Departure in YYYY, approximate delivery to SEPARATE-XXX dd-mm. 
            Expect an additional arrival announcement.
            """
        ),
    ),
    ("7", pgettext_lazy('novaposhta', "Arrived at the office.")),
    ("8", pgettext_lazy('novaposhta', "Arrived at the office.")),
    ("9", pgettext_lazy('novaposhta', "The dispatch has been received.")),
    (
        "10",
        pgettext_lazy(
            'novaposhta',
            """
            Send received% DateReceived%. 
            During the day, you will receive an S
            MS message about the receipt of a money transfer 
            and will be able to receive it at the box office of the "New Mail".
            """
        ),
    ),
    (
        "11",
        pgettext_lazy(
            'novaposhta',
            """
            Send received% DateReceived%.
            The money transfer is issued to the recipient.
            """
        ),
    ),
    (
        "14",
        pgettext_lazy(
            'novaposhta',
            "The shipment is sent to the receiver for review."
        )
    ),
    ("101", pgettext_lazy('novaposhta', "On the way to the recipient.")),
    ("102", pgettext_lazy('novaposhta', "Refusal of the recipient.")),
    ("103", pgettext_lazy('novaposhta', "Refusal of the recipient.")),
    ("108", pgettext_lazy('novaposhta', "Refusal of the recipient.")),
    ("104", pgettext_lazy('novaposhta', "Address has been changed.")),
    ("105", pgettext_lazy('novaposhta', "Canceled storage.")),
    (
        "106",
        pgettext_lazy(
            'novaposhta',
            "Received and created EN return shipping."
        )
    ),
)

COUNTERPARTY_PRIVATE_PERSON_TYPE = 'PrivatePerson'


TRACK_DOCUMENT_STATUS_LIMIT = 100

# SERVICE TYPES
DOORS_DOORS = 'DoorsDoors'
DOORS_WAREHOUSE = 'DoorsWarehouse'
DOORS_POSTOMAT = 'DoorsPostomat'
WAREHOUSE_WAREHOUSE = 'WarehouseWarehouse'
WAREHOUSE_DOORS = 'WarehouseDoors'
WAREHOUSE_POSTOMAT = 'WarehousePostomat'

WAREHOUSE, DOORS, POSTOMAT = 'WAREHOUSE', 'DOORS', 'POSTOMAT'

WAREHOUSE_DEPARTURE_SERVICE_TYPES_MAP = {
    WAREHOUSE: WAREHOUSE_WAREHOUSE,
    DOORS: WAREHOUSE_DOORS,
    POSTOMAT: WAREHOUSE_POSTOMAT,
}

DOORS_DEPARTURE_SERVICE_TYPES_MAP = {
    WAREHOUSE: DOORS_WAREHOUSE,
    DOORS: DOORS_DOORS,
    POSTOMAT: DOORS_POSTOMAT,
}

DEFAULT_DESCRIPTION = 'Description'
RECIPIENT_REF = 'Recipient'
DEFAULT_PAYER_TYPE = "Sender"
DEFAULT_PAYMENT_METHOD = "Cash"
DEFAULT_CARGO_TYPE = "Parcel"
DEFAULT_SERVICE_TYPE = WAREHOUSE_WAREHOUSE
DEFAULT_COUNTERPARTY_TYPE = COUNTERPARTY_PRIVATE_PERSON_TYPE
DEFAULT_BACKWARD_DELIVERY_CARGO_TYPE = "Money"
DEFAULT_DOCUMENT_WEIGHT = 0.1

WAREHOUSE_PACKSTATION_TYPE_REF = 'f9316480-5f2d-425d-bc2c-ac7cd29decf0'
NOVAPOSHTA_WEIGHT_VOLUME_RATIO = 250
NOVAPOSHTA_MINIMAL_ESTIMATED_COST = 300

NOVAPOSHTA_REQUEST_THROTTLING_DEFAULT_TIME = 2

NOVAPOSHTA_TIME_ZONE = 'Europe/Kiev'
