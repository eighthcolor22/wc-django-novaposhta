from abc import ABC, abstractmethod
from typing import Dict, NamedTuple

import requests

from .consts import API_V2_URL, JSON_RESPONSE_CONTENT_TYPE
from .exceptions import NovaposhtaResponseError
from .signals import api_request_signal


__all__ = (
    'Response',
    'BaseRequestClient',
    'SyncRequestClient',
)


Response = NamedTuple('Response', [
    ('status_code', int),
    ('request_url', str),
    ('request_data', Dict),
    ('response_data', Dict),
])


class BaseRequestClient(ABC):
    """
    Base class to work with remote API server. Use this one in your
    APIClient's variants.
    """
    API_BASE_URL = API_V2_URL

    def __init__(self, api_url: str = None):
        """
        Initializes api connector. Received parameters required to
        connect remote server.

        Args:
            api_url (str, optional): Base url used to construct url.
        """
        self.api_url = api_url or self.API_BASE_URL

    def construct_url(self, *args) -> str:
        """
        Returns url with joined args as parts of url.

        Args:
            *args: part of url.

        Returns:
            str: URL
        """
        url = self.api_url

        if not args:
            return url

        joined_args = '/'.join([x.strip('/') for x in args])

        return f'{url}{joined_args}'

    def get_base_headers(self) -> Dict:
        """
        Return dictionary of HTTP Headers to send with your request
        """
        return {'Content-Type': 'application/json'}

    @abstractmethod
    def make_request(self, **kwargs) -> 'Response':
        raise NotImplementedError


class SyncRequestClient(BaseRequestClient):
    def make_request(
            self,
            method: str,
            path: str = '',
            data: Dict = None,
            json: Dict = None,
            files: Dict = None,
            headers: Dict = None,
            timeout: int = 20
    ) -> 'Response':
        """
        Private method used to send request to the remote REST API
        server.

        Args:
            method (str): REST method to use.
            path (str): Corresponding relative path to send request.
            data (Dict, optional): Params to send.
            json (Dict, optional): json to send.
            files (Dict, optional): Files to send.
            headers (Dict, optional): Request headers.
            timeout (int): request timeout
        Returns:
            Response: requests' response instance.

        Raises:
            AttributeError: Unsupported method was used.
        """
        url = self.construct_url(path)

        request_method = getattr(requests, method.lower(), None)

        if not request_method:
            raise AttributeError(f'{method} is not supported')

        if headers is None:
            headers = {}

        headers.update(self.get_base_headers())

        # Delete method accepts only path, without extra params
        if method == 'delete':
            response = request_method(
                url=url,
                headers=headers,
                timeout=timeout
            )
        else:
            response = request_method(
                url,
                data,
                json=json,
                files=files,
                headers=headers,
                timeout=timeout
            )

        response_content_type = response.headers['content-type']
        if response_content_type != JSON_RESPONSE_CONTENT_TYPE:
            raise NovaposhtaResponseError(str(response._content))

        response = Response(
            status_code=response.status_code,
            request_url=url,
            request_data=data or json,
            response_data=response.json(),
        )

        api_request_signal.send(
            sender="NovaPoshta",
            source=response.request_url,
            method=method,
            status_code=response.status_code,
            request_data=response.request_data,
            response=response.response_data,
            user_id=None
        )

        return response
